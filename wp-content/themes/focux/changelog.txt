== Change Log ==

** V1.01 **

- Fixed some CSS issues.
- Enabled editor style.
- Fixed the post featured image is cut off on mobile phone.
- Limited the height of embed video in the post.
- Increased the font size of the post content.
- Added Child Theme.
- Upgraded Revolution Slider to 5.2.5.1
- Upgraded ThemeVan Shortcakes plugin, you can download it here: https://github.com/themevan/themevan-shortcodes/archive/master.zip

  Changed Files
  - assets/css/main.css
  - inc/init.php
  - editor-style.css
  - style.css

** V1.02 **

- Changed the topbar appearance way.
- Fixed the topbar overlapped by the admin bar.
- Fixed the background color of sub menu can't be changed by customizer.
- Added custom LOGO option for each page.
- Max Mega Menu plugin Ready.

  Changed Files
  - assets/css/main.css
  - assets/js/theme.js
  - inc/functions/template-tags.php
  - inc/functions/metabox.php
  - inc/functions/extra.php
  - inc/customizer/customizer.php
  - inc/init.php
  - style.css

** V1.03 **

- Fixed the LOGO doesn’t display in 404 page.
- Added ‘sticky topbar’ option into Appearance > Customize.
- Added ‘Additional Code’ fields into Appearance > Customize.
- Improved some small CSS issues.
- Upgraded ThemeVan Shortcodes plugin to 1.0.4 (Need to reinstall, you can download it here: https://github.com/themevan/themevan-shortcodes)

  Changed Files
  - inc/functions/welcome/content/intro.php
  - inc/functions/welcome/css/welcome.css
  - inc/functions/template-tags.php
  - inc/customize/customize.php
  - inc/plugins/themevan-shortcodes.zip
  - languages/focux.pot
  - assets/css/main.css
  - assets/js/theme.js
  - style.css

** V1.04 **

- Fixed breadcrumbs bug.
- Fixed a responsive bug.
- Upgraded Revolution Slider to 5.2.5.4
- Upgraded Visual Composer to 4.12

  Changed Files
  - inc/functions/hooks.php
  - inc/functions/woocommerce/extra.php
  - assets/css/main.css
  - style.css


** V1.05 **

- Changed some CSS for compatible with WooCommerce 2.6
- Upgraded Revolution Slider to 5.2.6
- Improved some CSS and responsive issues
- Fixed the One Click Demo Import plugin can not be installed

  Changed Files
  - assets/css/woocommerce.css
  - inc/plugins/plugins.php
  - inc/plugins/revslider.zip
  - style.css


** V1.06 **

- Improved Account page style
- Improved some details design
- Added smooth scroll effect for mouse wheel
- Added one click demo import feature
- Upgraded Revolution Slider to 5.2.6
- Upgraded Visual Composer to 4.12.1
- Upgraded ThemeVan Shortcodes to 1.05

  Changed Files
  - assets/css/woocommerce.css
  - assets/css/main.css
  - assets/js/theme.js
  - inc/functions/welcome/content/intro.php
  - inc/plugins/plugins.php
  - inc/demo
  - style.css
  