<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * * @package Focux
 */

?><!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6 no-js" lang="en-US"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7 no-js" lang="en-US"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage"> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> 
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="body-container">
    <?php
      do_action('focux_before_header');
      
	  $header=get_header_image();
	  $header_class='';
	  $header_picture='';
      if(is_home() || is_front_page()){
        if(has_header_image()){
	      $header_class=" site-cover";
	      $header_picture=' style=background-image:url('.esc_url($header).');';
	    }
      }
    ?>
	<header id="masthead" class="site-header<?php echo esc_html($header_class);?>"<?php echo esc_attr($header_picture);?>>

		<?php
		/**
		 * Hook focux_header
		 * @hooked focux_before_navigation - 0
		 * @hooked focux_custom_logo - 10
		 * @hooked focux_primary_navigation - 20
		 * @hooked focux_top_buttons - 30
		 * @hooked focux_after_navigation - 40
		 * @hooked focux_cover - 50
		 */
		do_action('focux_header');
		?>
	</header><!-- #masthead -->
	
	<?php do_action('focux_after_header');?>
