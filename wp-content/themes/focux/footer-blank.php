<?php
/**
 * The blank footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package Focux
 */

?>	
<?php wp_footer(); ?>
</body>
</html>
