<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * * @package Focux
 */

if ( ! is_active_sidebar( 'sidebar-page' ) ) {
	return;
}
?>

<div id="secondary" class="fx-grid fx-col3 widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-page' ); ?>
</div><!-- #secondary -->
