<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * * @package Focux
 */
 
if(get_theme_mod( 'archive_sidebar')=='no' || null == get_theme_mod( 'archive_sidebar')){
	get_template_part( 'archive', 'no-sidebar' );
	die();
}
get_header(); ?>
	
	<?php
	  /* Hook: focux_before_content, focux_before_main_content
	   * @Hooked: focux_before_content()
	   * @Hooked: focux_before_main_content();
	   */
	  do_action('focux_before_content');
	  do_action('focux_before_main_content');
	?>

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h3 class="section-title">', '<span class="divider"><span></span></span></h3>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php echo focux_pagenavi(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		<?php
		  /* Hook: focux_after_main_content
		   * @Hooked: focux_after_main_content();
		   */
		  do_action('focux_after_main_content');
		?>

        <?php get_sidebar(); ?>
	
	    <?php
	    /* Hook: focux_after_content
	     * @Hooked: focux_after_content();
	     */
		 do_action('focux_after_content');	 
	    ?>
  
<?php get_footer(); ?>
