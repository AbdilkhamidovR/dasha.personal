<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * * @package Focux
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('narrow'); ?>>
    <a href="<?php echo esc_url( get_permalink() );?>" rel="bookmark" class="featured_thumbnail"><?php if(has_post_thumbnail()){the_post_thumbnail('large');}?></a>
	<header class="entry-header<?php echo focux_narrow_container(" narrow");?>">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php focux_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content<?php echo focux_narrow_container(" narrow");?>">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->
   
    <div class="divider<?php echo focux_narrow_container(" narrow");?>"></div>
   
	<footer class="entry-footer">
		<?php focux_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	
	<div class="divider"></div>
</article><!-- #post-## -->
