<?php
/**
 * Related Posts
 * @package Focux
 */
if ( get_theme_mod('related_posts') == 'yes' ) {
	if ( get_theme_mod('single_related_posts_show_by') == 'related_cat' ) {
		$focux_taxs = get_the_category( $post->ID ); // Display related posts by category
	} else {
		$focux_taxs = wp_get_post_tags( $post->ID ); // Display related posts by tag
	}
	
	
	if ( $focux_taxs ) {
	
		$focux_tax_ids = array();
	
		foreach($focux_taxs as $individual_tax) $focux_tax_ids[] = $individual_tax->term_id;
	
		$posts_to_show = 3;
	
		if ( get_theme_mod('single_related_posts_show_by') == 'related_cat' ) { 
			// Loop argumnetsnts show posts by category
			$args = array(
				'category__in' => $focux_tax_ids,
				'post__not_in' => array( $post->ID ),
				'posts_per_page' => $posts_to_show,
				'ignore_sticky_posts' => 1
			);
		} else { 
			// Loop argumnetsnts show posts by category
			$args = array(
				'tag__in' => $focux_tax_ids,
				'post__not_in' => array( $post->ID ),
				'posts_per_page' => $posts_to_show,
				'ignore_sticky_posts' => 1
			);
		}
	
		$focux_related_posts = new WP_Query( $args );
		 if( $focux_related_posts->have_posts() ) : 
	?>
		
	    <section class="related-posts <?php echo focux_narrow_container("narrow");?>">
	    
	        <h3 class="section-title"><?php esc_html_e( 'You may also like', 'focux' ); ?></h3>
	    
	        <div class="grids">
	            <?php 
					while ( $focux_related_posts->have_posts() ) : $focux_related_posts->the_post(); ?>
			
					<div class="item post">
						  <div class="thumbnail">
						      <a title="<?php esc_attr(get_the_title());?>" href="<?php echo esc_url(get_permalink());?>">
								<?php 
								if ( has_post_thumbnail() ) {
									the_post_thumbnail( 'medium' );
								}
								?></a>
						  </div>
						  <header class="entry-header">
							  <h6><a href="<?php echo esc_url(get_permalink());?>"><?php the_title(); ?></a></h6>
						  </header>
					</div>
				
					<?php endwhile; ?>
	            
	            	<?php wp_reset_postdata(); ?>
	                <div class="clearfix"></div>
	
	         </div>
	    </section>
	
	<?php 
	   endif;
	} ?>
<?php
 }	
?>