<?php
/**
 * Template: Overlap Posts Loop
 *
 * @package Focux
 */
?>             

        
		<!---Posts-->
		<?php
		  $ex_css='fx-grid fx-col-12 grid-post';
		  if(!has_post_thumbnail()){
		   $label=' no-thumbnail';
		   $truncate=600;
		  }else{
		   $label='';
		   $truncate=300;
		  }
		?>
		<article <?php post_class(esc_attr($ex_css));?>>
		 <?php
		 	if(has_post_thumbnail()){
			 	echo '<div class="thumbnail"><a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail(get_the_ID(),'large',array('class'=>'featured_image','alt'=>esc_attr(get_the_title()))).'</a></div>';
		 	}
		 ?>
		 <div class="entry-grid<?php echo esc_attr($label);?>">
		  <h6 class="category"><?php echo get_the_category_list( ', ' );?></h6>
		  <h4 class="entry-title"><a href="<?php echo esc_url(get_permalink());?>/#content" title="<?php the_title();?>"><?php the_title();?></a><span class="line"></span></h4>
		 
		  <div class="entry-content">
		    <p><?php echo focux_truncate(get_the_excerpt(),esc_attr($truncate));?></p>
		  </div><!--entry-content-->

		  <footer class="entry-meta">
		   <span class="date"><?php the_date();?></span>
		   <span class="author"><?php the_author_posts_link(); ?></span>
		  </footer><!--entry-tools-->

		 </div><!--entry-body-->

		</article><!--post-->
