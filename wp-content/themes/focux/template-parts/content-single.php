<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * * @package Focux
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if(has_post_thumbnail()):?>
	<a href="<?php echo esc_url( get_permalink() );?>" rel="bookmark" class="featured_thumbnail"><?php if(has_post_thumbnail()){the_post_thumbnail('large');}?></a>
	<?php endif;?>

	<header class="entry-header<?php echo focux_narrow_container(" narrow");?>">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php focux_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content<?php echo focux_narrow_container(" narrow");?>">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'focux' ),
				'after'  => '</div>',
			) );
		?>
		
	<?php
    //Tags
    $posttags=get_the_tags();
    if($posttags <>''):?>
    <div class="taglist">
      <i class="fa fa-tag"></i> <?php the_tags('',' '); ?>
    </div>  
    <?php endif;?>
    
	</div><!-- .entry-content -->
    
    
    
</article><!-- #post-## -->

