<?php
/**
 * Related Posts
 * @package Focux
 */
?>
<?php if ( get_theme_mod('post_author') == 'yes' ):?>
    <!--Author-->
	<section id="author_vcard" <?php echo focux_narrow_container('class="narrow"');?>>
	<h3 class="author-title section-title"><?php esc_html_e('About Author','focux');?></h3>
	<a class="avatar" href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ));?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 80 ); ?></a>
	<p><strong><?php the_author_meta('nickname'); ?></strong>	</p>
	<?php the_author_meta('description'); ?> 
	<p><span class="social_icons"><?php do_action('focux_author_social_profile');?></span></p>
	</section>
<?php endif;?>