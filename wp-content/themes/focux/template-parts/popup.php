<?php
/**
 * Popup
 * * @package Focux
 * @since 1.0
 */
?>

<?php if ( class_exists( 'woocommerce') ):?>
<div id="fx_popup">
  <a id="fx_popup_close">&times;</a>
  
  <!--Search-->
  <div id="fx_search" class="popup_content">
    <h3><?php esc_attr_e('Search Products','focux');?></h3>
	<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url(home_url( '/' ));?>" >
    <div>
    <input type="text" id="search_product" placeholder="<?php esc_attr_e('Search Products and knock Enter key.','focux');?>" value="<?php get_search_query();?>" name="s" />
    <input type="hidden" id="post_type" name="post_type" value="product" />
    </div>
    </form>
  </div>
  
  <!--Cart-->
  <div id="fx_cart" class="popup_content">
    <h3><?php esc_attr_e('Cart','focux');?></h3>
	 <div class="widget_shopping_cart_content"></div>
  </div>

</div>
<div class="fx_popup_overlay"></div>
<?php endif;?>