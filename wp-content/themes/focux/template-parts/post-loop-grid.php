<?php
/**
 * Template: Grid Posts Loop
 *
 * @package Focux
 * @subpackage Nib
 * @since 1.0
 */
?>             

        
		<!---Posts-->
		<?php
		  $ex_css='fx-grid fx-col6 grid-post';
		?>
		<article <?php post_class($ex_css);?>>
		
		 <div class="entry-grid<?php if(has_post_thumbnail()){echo' whiteText';}?>">
		 
		  <h4 class="entry-title">
		   <a href="<?php echo esc_url(get_permalink());?>/#content" title="<?php the_title();?>"><?php the_title();?></a><span class="line"></span>
		  </h4>
		 
		  <?php if(!has_post_thumbnail()):?>
		  <div class="entry-content">
		    <p><?php echo focux_truncate(get_the_excerpt(),200);?></p>
		  </div><!--entry-content-->

		  <footer class="entry-meta">
		   <?php focux_posted_on(); ?>
		  </footer><!--entry-tools-->
		  <?php endif;?>
		 </div><!--entry-body-->
		 
		 <?php if(has_post_thumbnail()):?>
		 <div class="thumbnail"><a href="<?php echo esc_url(get_permalink());?>" title="<?php the_title();?>"><?php the_post_thumbnail('large');?></a></div>
		 <?php endif;?>
		</article><!--post-->
