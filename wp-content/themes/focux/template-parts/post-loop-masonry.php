<?php
/**
 * Template: Posts Masonry Loop
 *
 * @package Focux
 */
?>             
		<!---Posts-->
		<?php
		  $ex_css='fx-grid fx-col6 grid-post';
		?>
		<article <?php post_class($ex_css);?>>
		
		<div class="entry-body container">
		  <h4 class="entry-title"><a href="<?php echo esc_url(get_permalink());?>/#content" title="<?php the_title();?>"><?php the_title();?></a></h4>
		   <div class="separator"></div>
	       <div class="entry-meta">
			<?php focux_posted_on(); ?>
		   </div><!-- .entry-meta -->
		
		 
		 <?php if(has_post_thumbnail()):?>
		 <div class="thumbnail"><a href="<?php echo esc_url(get_permalink());?>" title="<?php the_title();?>"><?php the_post_thumbnail('large');?></a></div>
		 <?php endif;?>

		
		  <div class="entry-content">
		    <?php 
		      the_excerpt();
		    ?>
            <div class="clear"></div>
		  </div><!--entry-content-->

		  <footer class="entry-tools">
		     <a href="<?php echo esc_url(get_permalink());?>" title="<?php the_title();?>"><?php esc_html_e('Continue to read','focux');?> &rarr;</a>
		  </footer><!--entry-tools-->
		 </div><!--entry-body-->
		</article><!--post-->