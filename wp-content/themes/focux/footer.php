<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * * @package Focux
 */

?>	
 </div><!-- #page -->

<?php if ( is_active_sidebar( 'bottom-widget-1' ) ):?>
<div id="site-bottom" class="site-bottom">
<div class="fx-grid-1000">
  <div class="bottom-widget fx-grid fx-col3">
      <?php dynamic_sidebar( 'bottom-widget-1' ); ?>
  </div>
  <div class="bottom-widget fx-grid fx-col3">
      <?php dynamic_sidebar( 'bottom-widget-2' ); ?>
  </div>
  <div class="bottom-widget fx-grid fx-col3">
      <?php dynamic_sidebar( 'bottom-widget-3' ); ?>
  </div>
  <div class="bottom-widget fx-grid fx-col3 last">
      <?php dynamic_sidebar( 'bottom-widget-4' ); ?>
  </div>
</div>
</div>
<?php endif;?>

<footer id="colophon" class="site-footer">
<div class="site-info fx-grid-1000">
  <?php
    /**
     * Hook: focux_footer
	 * @Hooked  focux_copyright()
     */
	do_action('focux_footer');
  ?>
</div><!-- .site-info -->
</footer><!-- #colophon -->

<?php 
 /* Popup */
 get_template_part( 'template-parts/popup');
 
 /* Mobile Menu
  * It will only display at the right side when view the website on mobile.
  */
 get_template_part( 'template-parts/mobile-menu');
?>

<?php wp_footer(); ?>
</body>
</html>
