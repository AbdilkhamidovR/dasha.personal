/**
 * customizer.js
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );
	
	wp.customize( 'heading_font', function( value ){
		value.bind( function( to ){
			$( 'body' ).append( '<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=' + to + '">' );
			
			$( 'h1,h2,h3,h4,h5,h6,blockquote').css( 'font-family', '"' + to + '"' );
		} );
	} );
	
	wp.customize( 'logo_font', function( value ){
		value.bind( function( to )
		{
			$( 'body' ).append( '<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=' + to + '">' );
			
			$( '.site-branding .site-title').css( 'font-family', '"' + to + '"' );
		} );
	} );
	
	wp.customize( 'navigation_font', function( value ){
		value.bind( function( to ){
			$( 'body' ).append( '<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=' + to + '">' );
			
			$( '.main-navigation a').css( 'font-family', '"' + to + '"' );
		} );
	} );
	
	wp.customize( 'body_font', function( value ){
		value.bind( function( to ){
			$( 'body' ).append( '<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=' + to + '">' );
			
			$( 'body').css( 'font-family', '"' + to + '"' );
		} );
	} );
	
	wp.customize( 'global_color', function( value ){
		value.bind( function( to ){	
			$('a,#fx-topbar.withbg .main-navigation a:hover,.main-navigation a:hover,.site-cover .main-navigation a:hover,.widget li a:hover,.widget_categories ul.children li a:hover,.widget_nav_menu ul.sub-menu li a:hover,.entry-title a:hover,.woocommerce ul.products li.product .price,#site-icons a:hover,.woocommerce div.product p.price, .woocommerce div.product span.price,.custom_price_table.featured header h3, .custom_price_table.featured header .price').css( 'color', '"' + to + '"' );
			
			$('input[type="button"]:hover,input[type="submit"]:hover,input[type="reset"]:hover,.button:hover,.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover,#site-icons a:hover,.custom_price_table.featured header h3, .custom_price_table.featured header .price').css( 'border-color', '"' + to + '"' );
			
            $('.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,input[type="button"], input[type="submit"],input[type="reset"], .button,input[type="button"]:hover,input[type="submit"]:hover,input[type="reset"]:hover,.button:hover,.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover,.swiper-pagination-bullet-active,.bullet_links a.active').css( 'background-color', '"' + to + '"' );
		} );
	} );
	
	wp.customize( 'nav_bg', function( value ){
		value.bind( function( to ){	
			$('.site-header,#fx-topbar').css( 'background-color', '"' + to + '"' );
		} );
	} );
	
	wp.customize( 'nav_link_color', function( value ){
		value.bind( function( to ){	
			$('#fx-topbar a').css( 'background-color', '"' + to + '"' );
		} );
	} );
	
} )( jQuery );
