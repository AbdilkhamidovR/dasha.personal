<?php
/**
 * Template Name: Standard Blog Template
 *
 * This is the template that displays the blog posts as standard layout
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Focux
 */

get_header(); ?>

	 <?php
	  /* Hook: focux_before_content
	   * @Hooked: focux_before_content()
	   */
	  do_action('focux_before_content');
	?>

		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>

			<?php /* Start the Loop */ ?>
			<?php					  
               $limit = get_option('posts_per_page');
		       if(!is_front_page()){
		         $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		       }else{		       
		         $paged = (get_query_var('page')) ? get_query_var('page') : 1;
		       }
		       query_posts(array('post_type'=>'post','posts_per_page'=>$limit,'paged'=>$paged));
			?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
				   get_template_part( 'template-parts/content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php echo focux_pagenavi(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

	
	    <?php
	    /* Hook: focux_after_content
	     * @Hooked: focux_after_content();
	     */
		 do_action('focux_after_content');	 
	    ?>
<?php get_footer(); ?>
