<?php
/**
 * Template Name: Transparent Header Page
 *
 * This is the template that displays Custom content.
 * @package Focux
 */
get_header(); ?>
	
	<div id="page" class="hfeed site">
	  <div id="content" class="site-content">
		<?php 
		    while ( have_posts() ) : the_post(); 
			 the_content(); 	
		    endwhile; // End of the loop. 
		    
		    focux_bullet_links();
		?>
	  </div>
	</div>
	
<?php get_footer(); ?>