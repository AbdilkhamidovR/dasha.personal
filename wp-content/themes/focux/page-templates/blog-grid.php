<?php
/**
 * Template Name: Grid Blog Template
 *
 * This is the template that displays the blog posts as grid layout
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Focux
 */

get_header(); ?>

	<?php
	  /* Hook: focux_before_content, focux_before_main_content
	   * @Hooked: focux_before_content()
	   */
	  do_action('focux_before_content');
	?>
	        <div class="grid_blog">
               <?php               
               /*Start Loop*/
               $limit = get_option('posts_per_page');
		       if(!is_front_page()){
		         $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		       }else{		       
		         $paged = (get_query_var('page')) ? get_query_var('page') : 1;
		       }
		       query_posts(array('post_type'=>'post','posts_per_page'=>$limit,'paged'=>$paged));
               while ( have_posts() ) : the_post();
                  get_template_part( 'template-parts/post', 'loop-grid');
               endwhile;
               ?>
               <?php echo focux_pagenavi(); ?>
	        </div>

    <?php
	  /* Hook: focux_after_content
	   * @Hooked: focux_after_content()
	   */
	  do_action('focux_after_content');
	?>
<?php get_footer(); ?>