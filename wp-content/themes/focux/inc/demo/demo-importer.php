<?php
/*Import data*/
if ( ! function_exists( 'focux_import_files' ) ) :
function focux_import_files() {
    return array(
        array(
            'import_file_name'             => 'Default Demo',
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo/default/content.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo/default/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . 'screenshot.png',
            'import_notice'                => __( 'Please waiting for a few minutes, do not close the window or refresh the page until the data is imported.', 'alaya' ),
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'focux_import_files' );
endif;


if ( ! function_exists( 'focux_after_import' ) ) :
function focux_after_import( $selected_import ) {
    //Set Menu
    $top_menu = get_term_by('name', 'Primary Menu', 'nav_menu');
    set_theme_mod( 'nav_menu_locations' , 
        array( 
              'primary' => $top_menu->term_id, 
              'mobile' => $top_menu->term_id 
             ) 
    );

    //Set Front page
    $page = get_page_by_title( 'Home V1');
    if ( isset( $page->ID ) ) {
        update_option( 'page_on_front', $page->ID );
        update_option( 'show_on_front', 'page' );
    }

    if ( class_exists( 'RevSlider' ) ) {
       $slider_array = array(
              get_template_directory()."/inc/demo/default/home-slider-1.zip",
              get_template_directory()."/inc/demo/default/home-slider-2.zip",
              get_template_directory()."/inc/demo/default/home-slider-3.zip",
              get_template_directory()."/inc/demo/default/home-slider-4.zip",
              get_template_directory()."/inc/demo/default/about-slider.zip",
              get_template_directory()."/inc/demo/default/shop-category.zip"
              );

      $slider = new RevSlider();
       
      foreach($slider_array as $filepath){
       $slider->importSliderFromPost(true,true,$filepath);  
      }
       
      echo ' Slider processed';
    }
}
add_action( 'pt-ocdi/after_import', 'focux_after_import' );
endif;