<?php
/**
 * Focux Theme Customizer.
 * * @package Focux
 */


/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function focux_customize_register( $wp_customize ) {
    /* Remove Default Options */
    $wp_customize->remove_control('header_textcolor');
    $wp_customize->remove_control('background_color');
    $wp_customize->remove_section('background_image');
    $wp_customize->remove_control('background_image');
    
    /* Include Google Font Lists*/
    include_once 'fonts.php';
    
    /* LOGO Uploader */
	$wp_customize->add_setting( 
	   'logo_upload', 
	   array(	
	      'default' => '',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
	   ) 
	);
	
	$wp_customize->add_control( 
	    new WP_Customize_Upload_Control( 
		$wp_customize, 
		'logo_upload', 
		array(
			'label'      => esc_html__( 'Custom LOGO', 'focux' ),
			'description' => esc_html__( 'Your theme recommends a LOGO max size of 280 x 60 pixels.','focux'),
			'section'    => 'title_tagline',
			'settings'   => 'logo_upload',
		) ) 
	);
	
	/* Copyright Text */
	$wp_customize->add_setting( 
	   'copyright', 
	   array(	
	      'default' => '',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
	   ) 
	);
																		
	$wp_customize->add_control( 
	   'control_copyright', 
	    array(
	      'label' => esc_html__('Copyright Text','focux'),
		  'section' => 'title_tagline',
		  'settings' => 'copyright',
		  'type' => 'text'
		) 
	);
	
	
	/**
     * General Section
     */
    $wp_customize->add_section(
	    'general_section',
	    array(
	        'title' => esc_html__('General','focux'),
	        'priority' => 20,
	    )
	);
	
	$wp_customize->add_setting( 
	    'sticky_top', 
	    array(	
	      'default' => 'no',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
		) 
	);
																		
	$wp_customize->add_control( 
	     'sticky_top', 
	     array(	
	       'label' => esc_html__('Sticky Topbar?','focux'),
		   'section' => 'general_section',
		   'settings' => 'sticky_top',
		   'type' => 'select',
		   'choices' => array(
		     'no' => esc_html__('No','focux'),
			 'yes' => esc_html__('Yes','focux')
		   )
		 ) 
	);
	

	/**
     * Color Section
     */
    $wp_customize->add_section(
	    'color_section',
	    array(
	        'title' => esc_html__('Colors','focux'),
	        'description' => esc_html__( 'You can change the color scheme below.','focux'),
	        'priority' => 20,
	    )
	);
	
	/* ## Global Color */
	$wp_customize->add_setting( 
	    'global_color', 
	    array(	
	      'default' => '#1bbff5',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'sanitize_hex_color'
		) 
	);
																		
	$wp_customize->add_control( 
	    new WP_Customize_Color_Control( 
	      $wp_customize,
		  'control_global_color', 
		  array(  
		    'label' => esc_html__( 'Global Color', 'focux' ),												 	
		    'section' => 'color_section',
		    'settings' => 'global_color' 
		 ) 
	   ) 
	);
	
	/* ## Background color of Navigation bar */
	$wp_customize->add_setting( 
	    'nav_bg', 
	    array(	
	      'default' => '#000000',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'sanitize_hex_color'
		) 
	);
																		
	$wp_customize->add_control( 
	    new WP_Customize_Color_Control( 
	      $wp_customize,
		  'control_nav_bg', 
		  array(  
		    'label' => esc_html__( 'Background Color of Navigation Bar', 'focux' ),												 	
		    'section' => 'color_section',
		    'settings' => 'nav_bg' 
		 ) 
	   ) 
	);
	
	/* ## Navigation link color*/
	$wp_customize->add_setting( 
	    'nav_link_color', 
	    array(	
	      'default' => '#ffffff',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'sanitize_hex_color'
		) 
	);
																		
	$wp_customize->add_control( 
	    new WP_Customize_Color_Control( 
	      $wp_customize,
		  'control_nav_link_color', 
		  array(  
		    'label' => esc_html__( 'Navigation Link Color', 'focux' ),												 	
		    'section' => 'color_section',
		    'settings' => 'nav_link_color' 
		 ) 
	   ) 
	);
    
    /**
     * Font Section
     */
    $wp_customize->add_section(
	    'font_section',
	    array(
	        'title' => esc_html__('Fonts','focux'),
	        'description' => esc_html__( 'You can change the other font from the following google fonts list.','focux'),
	        'priority' => 25,
	    )
	);
	
	/* Logo Text Font*/
	$wp_customize->add_setting( 
	    'logo_font', 
	    array(	
	      'default' => 'Karla',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
		) 
	);
																		
	$wp_customize->add_control( 
	     'control_logo_font', 
	     array(	
	       'label' => esc_html__('LOGO Text Font','focux'),
		   'section' => 'font_section',
		   'settings' => 'logo_font',
		   'type' => 'select',
		   'choices' => $all_fonts 
		 ) 
	);
	
	/* Heading Text Font*/
	$wp_customize->add_setting( 
	    'heading_font', 
	    array(	
	      'default' => 'Open Sans',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
		) 
	);
																		
	$wp_customize->add_control( 
	     'control_heading_font', 
	     array(	
	       'label' => esc_html__('Heading Text Font','focux'),
		   'section' => 'font_section',
		   'settings' => 'heading_font',
		   'type' => 'select',
		   'choices' => $all_fonts 
		 ) 
	);
	
	/* Navigation Text Font*/
	$wp_customize->add_setting( 
	    'navigation_font', 
	    array(	
	      'default' => 'Karla',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
		) 
	);
																		
	$wp_customize->add_control( 
	     'control_navigation_font', 
	     array(	
	       'label' => esc_html__('Navigation Text Font','focux'),
		   'section' => 'font_section',
		   'settings' => 'navigation_font',
		   'type' => 'select',
		   'choices' => $all_fonts 
		 ) 
	);
	
	/* Body Text Font*/
	$wp_customize->add_setting( 
	    'body_font', 
	    array(	
	      'default' => 'Lato',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
		) 
	);
																		
	$wp_customize->add_control( 
	     'control_body_font', 
	     array(	
	       'label' => esc_html__('Body Text Font','focux'),
		   'section' => 'font_section',
		   'settings' => 'body_font',
		   'type' => 'select',
		   'choices' => $all_fonts 
		 ) 
	);
	
	/**
     * Sidebar Section
     */
    $wp_customize->add_section(
	    'sidebar_section',
	    array(
	        'title' => esc_html__('Sidebar','focux'),
	        'description' => esc_html__( 'Manage the sidebar for blog and shop pages.','focux'),
	        'priority' => 80,
	    )
	);
	
	/*Shop Sidebar*/
	if ( class_exists( 'woocommerce' ) ){
		$wp_customize->add_setting( 
		    'shop_sidebar', 
		    array(	
		      'default' => 'no',
			  'transport' => 'refresh',
			  'sanitize_callback' => 'esc_attr'
			) 
		);
																			
		$wp_customize->add_control( 
			  'shop_sidebar', 
			  array(  
			    'type' => 'select',
			    'label' => esc_html__( 'Shop Page Sidebar', 'focux' ),									
			    'section' => 'sidebar_section',
			    'choices' => array(
				            'no' => esc_html__('No','focux'),
				            'yes' => esc_html__('Yes','focux')
				             ),
			 ) 
		);
	}
	
	/*Blog Archive Sidebar*/
	$wp_customize->add_setting( 
		    'archive_sidebar', 
		    array(	
		      'default' => 'no',
			  'transport' => 'refresh',
			  'sanitize_callback' => 'esc_attr'
			) 
		);
																			
	$wp_customize->add_control( 
		  'archive_sidebar', 
		  array(  
		    'type' => 'select',
		    'label' => esc_html__( 'Blog Archive Sidebar', 'focux' ),								
		    'section' => 'sidebar_section',
		    'choices' => array(
				            'no' => esc_html__('No','focux'),
				            'yes' => esc_html__('Yes','focux')
				             ),
		 )
	);
	
	/*Post Sidebar*/
	$wp_customize->add_setting( 
		    'post_sidebar', 
		    array(	
		      'default' => 'no',
			  'transport' => 'refresh',
			  'sanitize_callback' => 'esc_attr'
			) 
	);
																			
	$wp_customize->add_control( 
		  'post_sidebar', 
		  array(  
		    'type' => 'select',
		    'label' => esc_html__( 'Post Sidebar', 'focux' ),											
		    'section' => 'sidebar_section',
		    'choices' => array(
				            'no' => esc_html__('No','focux'),
				            'yes' => esc_html__('Yes','focux')
				             ),
		 ) 
	);
	
	/**
     * Post Section
     */
    $wp_customize->add_section(
	    'post_section',
	    array(
	        'title' => esc_html__('Post','focux'),
	        'description' => esc_html__( 'Manage the sections for blog post page.','focux'),
	        'priority' => 90,
	    )
	);
	
	/*Related Posts*/
	$wp_customize->add_setting( 
	    'related_posts', 
	    array(	
	      'default' => 'no',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
		) 
	);
																		
	$wp_customize->add_control( 
		  'related_posts', 
		  array(  
		    'type' => 'select',
		    'label' => esc_html__( 'Related Posts', 'focux' ),											
		    'section' => 'post_section',
		    'choices' => array(
				            'no' => esc_html__('No','focux'),
				            'yes' => esc_html__('Yes','focux')
				             ),
		 ) 
	);
	
	/*Related Posts show by */
	$wp_customize->add_setting( 
	    'single_related_posts_show_by', 
	    array(	
	      'default' => 'no',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
		) 
	);
																		
	$wp_customize->add_control( 
		  'single_related_posts_show_by', 
		  array(  
		    'type' => 'select',
		    'label' => esc_html__( 'Related Posts Show By', 'focux' ),									
		    'section' => 'post_section',
		    'choices' => array(
			            'related_cat' => esc_html__('Related Category','focux'),
			            'related_tag' => esc_html__('Related Tag','focux')
			             ),
		 ) 
	);
	
	/*Post Author*/
	$wp_customize->add_setting( 
	    'post_author', 
	    array(	
	      'default' => 'no',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'esc_attr'
		) 
	);
																		
	$wp_customize->add_control( 
		  'post_author', 
		  array(  
		    'type' => 'select',
		    'label' => esc_html__( 'Author Section', 'focux' ),										
		    'section' => 'post_section',
		    'choices' => array(
				            'no' => esc_html__('No','focux'),
				            'yes' => esc_html__('Yes','focux')
				             ),
		 ) 
	);
	
	/**
     * Custom Code Section
     */
    $wp_customize->add_section(
	    'custom_code_section',
	    array(
	        'title' => esc_html__('Custom Code','focux'),
	        'description' => esc_html__( 'Just put your own custom code here','focux'),
	        'priority' => 90,
	    )
	);
	
	/* Custom CSS */
	$wp_customize->add_setting( 
	   'custom_css', 
	   array(	
	      'default' => '',
		  'transport' => 'postMessage',
		  'sanitize_callback' => 'sanitize_text_field'
	   ) 
	);
																		
	$wp_customize->add_control( 
	   'custom_css', 
	    array(
	      'label' => esc_html__('Custom CSS','focux'),
		  'section' => 'custom_code_section',
		  'settings' => 'custom_css',
		  'type' => 'textarea'
		) 
	);
	
	/* Custom javascript */
	$wp_customize->add_setting( 
	   'custom_js', 
	   array(	
	      'default' => '',
		  'transport' => 'postMessage',
		  'sanitize_callback' => 'sanitize_text_field'
	   ) 
	);
																		
	$wp_customize->add_control( 
	   'custom_js', 
	    array(
	      'label' => esc_html__('Custom Javascript','focux'),
		  'section' => 'custom_code_section',
		  'settings' => 'custom_js',
		  'type' => 'textarea'
		) 
	);
	
	/**
     * Additional Code Section
     */
    $wp_customize->add_section(
	    'additional_code_section',
	    array(
	        'title' => esc_html__('Additional Code','focux'),
	        'description' => esc_html__( 'You can add Google Analytics codes, meta information or import CSS\JS file below.','focux'),
	        'priority' => 91,
	    )
	);
	
	/* Head code */
	$wp_customize->add_setting( 
	   'head_code', 
	   array(	
	      'default' => '',
		  'transport' => 'postMessage',
		  'sanitize_callback' => 'sanitize_text_field'
	   ) 
	);
																		
	$wp_customize->add_control( 
	   'head_code', 
	    array(
	      'label' => esc_html__('Head Code','focux'),
		  'section' => 'additional_code_section',
		  'settings' => 'head_code',
		  'type' => 'textarea'
		) 
	);
	
	/* Footer Code */
	$wp_customize->add_setting( 
	   'footer_code', 
	   array(	
	      'default' => '',
		  'transport' => 'postMessage',
		  'sanitize_callback' => 'sanitize_text_field'
	   ) 
	);
																		
	$wp_customize->add_control( 
	   'footer_code', 
	    array(
	      'label' => esc_html__('Footer Code','focux'),
		  'section' => 'additional_code_section',
		  'settings' => 'footer_code',
		  'type' => 'textarea'
		) 
	);


	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';


}
add_action( 'customize_register', 'focux_customize_register' );

/**
 * Import Google Fonts	
 */
function focux_custom_fonts_url() {
        $fonts_url = '';
	    $font_families = array();
	 
	    if(get_theme_mod( 'navigation_font', 'Karla' )<>'Karla'){
	        $font_families[] = get_theme_mod( 'navigation_font', 'Karla' ).':400,600,700,900,400italic,600italic,700italic,900italic';
	    }
	    
	    if(get_theme_mod( 'logo_font', 'Karla' )<>'Karla'){
		    $font_families[] = get_theme_mod( 'logo_font', 'Karla' ).':400,600,700,900,400italic,600italic,700italic,900italic';
	    }
	 
	    if(get_theme_mod( 'body_font', 'Lato' )<>'Lato') {
	        $font_families[] = get_theme_mod( 'body_font', 'Lato' ).':400,700,400italic,700italic';
	    }
	    
	    if(get_theme_mod( 'heading_font', 'Open Sans' )<>'Open Sans'){
	        $font_families[] = get_theme_mod( 'heading_font', 'Lato' ).':400,700,800,300,100';
	    }
   
	    $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext,vietnamese,cyrillic-ext,cyrillic,greek,greek-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    
    return esc_url_raw( $fonts_url );
}

function focux_custom_font_styles() {
    wp_enqueue_style( 'focux-custom-fonts', focux_custom_fonts_url(), array(), null );
}

if ( get_theme_mod( 'navigation_font', 'Karla' )<>'Karla' || get_theme_mod( 'body_font', 'Lato' )<>'Lato' || get_theme_mod( 'logo_font', 'Karla' )<>'Karla' || get_theme_mod( 'heading_font', 'Open Sans' )<>'Open Sans' ) {
  add_action( 'wp_enqueue_scripts', 'focux_custom_font_styles' );
}


/**
 * Output Customize CSS	
 */
function focux_customize_css(){
	if ( get_theme_mod('global_color', '#1bbff5') <>'#1bbff5' || get_theme_mod('nav_bg', '#000000') <>'#000000' || get_theme_mod( 'navigation_font', 'Karla' )<>'Karla' || get_theme_mod( 'body_font', 'Lato' )<>'Lato' || get_theme_mod( 'logo_font', 'Montserrat' )<>'Montserrat' || get_theme_mod( 'heading_font', 'Karla' )<>'Karla' || get_theme_mod( 'custom_css')<>'' ):
?>
     <style type="text/css">
     /*CSS generated by wp customizer*/
     <?php if ( get_theme_mod('body_font', 'Lato') <>'Lato'):?>
         body { font-family:'<?php echo esc_attr(get_theme_mod('body_font', 'Lato')); ?>'; }
     <?php endif;?>
     <?php if ( get_theme_mod('heading_font', 'Open Sans') <>'Open Sans'):?>
         h1,h2,h3,h4,h5,h6,blockquote{font-family: '<?php echo esc_attr(get_theme_mod('heading_font', 'Karla')); ?>', serif;}
     <?php endif;?>
     <?php if ( get_theme_mod('navigation_font', 'Karla') <>'Karla'):?>
         .main-navigation a {font-family: '<?php echo esc_attr(get_theme_mod('navigation_font', 'Karla')); ?>', serif;}
     <?php endif;?>
     <?php if ( get_theme_mod('logo_font', 'Karla') <>'Karla'):?>
         .site-branding .site-title{font-family: '<?php echo esc_attr(get_theme_mod('logo_font', 'Karla')); ?>', serif;}
     <?php endif;?>
     
     <?php if ( get_theme_mod('nav_bg', '#000000') <>'#000000'):?>   
     	#fx-topbar,
     	#fx-topbar.nav-bg,
     	.main-navigation ul ul,
     	#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-megamenu > ul.mega-sub-menu,
     	#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-flyout ul.mega-sub-menu,
     	#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item.mega-toggle-on > a.mega-menu-link, 
#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link:hover, 
#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link:focus{background-color:<?php echo esc_attr(get_theme_mod('nav_bg', '#000000'));?>}
     <?php endif;?>
     
     <?php if ( get_theme_mod('nav_link_color', '#ffffff') <>'#ffffff'):?>   
     	#fx-topbar a,
     	#fx-topbar.nav-bg a,
     	.menu-toggle,
     	.main-navigation ul ul a,
     	#fx-topbar .main-navigation ul ul a,
     	#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link,
     	#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link:hover,
     	#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item,
		#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item > a.mega-menu-link,
		#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item #wp-calendar caption,
		#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-megamenu > ul.mega-sub-menu > li.mega-menu-item h4.mega-block-title,
		#mega-menu-wrap-primary #mega-menu-primary ul, 
		#mega-menu-wrap-primary #mega-menu-primary li, 
		#mega-menu-wrap-primary #mega-menu-primary p,
		#mega-menu-wrap-primary #mega-menu-primary img, 
		#mega-menu-wrap-primary #mega-menu-primary div, 
		#mega-menu-wrap-primary #mega-menu-primary a,
		#mega-menu-wrap-primary #mega-menu-primary a.mega-menu-link,
		#mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item a.mega-menu-link{color:<?php echo esc_attr(get_theme_mod('nav_link_color', '#ffffff'));?>}
     <?php endif;?>
     
     <?php if ( get_theme_mod('global_color', '#1bbff5') <>'#1bbff5'):?>    
         a,#fx-topbar.nav-bg a, .main-navigation a:hover,.main-navigation a:hover,.site-cover .main-navigation a:hover,.widget li a:hover,.widget_categories ul.children li a:hover,
.widget_nav_menu ul.sub-menu li a:hover,.entry-title a:hover,.woocommerce ul.products li.product .price,.woocommerce .star-rating,.woocommerce div.product p.price, .woocommerce div.product span.price,.custom_price_table.featured header h3{color:<?php echo esc_attr(get_theme_mod('global_color', '#1bbff5')); ?>;}

        .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,input[type="button"], input[type="submit"], input[type="reset"], .button,input[type="button"]:hover,input[type="submit"]:hover,input[type="reset"]:hover,.button:hover,.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover,.swiper-pagination-bullet-active{border-color:<?php echo esc_attr(get_theme_mod('global_color', '#1bbff5')); ?>;background-color:<?php echo esc_attr(get_theme_mod('global_color', '#1bbff5')); ?>;}
        
         #site-icons a:hover,.custom_price_table.featured header h3, .custom_price_table.featured header .price,.bullet_links a.active{border-color:<?php echo esc_attr(get_theme_mod('global_color', '#1bbff5')); ?>;color:<?php echo esc_attr(get_theme_mod('global_color', '#1bbff5')); ?>;}   
      <?php endif;?>
      
      <?php if ( get_theme_mod('sticky_top') =='yes'):?> 
        #fx-topbar.nav-up, 
        .page-template-page-transparent-header.admin-bar #fx-topbar.nav-up{top:0;} 
		#fx-topbar{position:fixed; top:0; left:0; }
      <?php endif;?>
      
      <?php echo get_theme_mod( 'custom_css');?>
     </style>
<?php
  endif;
  
  echo get_theme_mod('head_code').PHP_EOL;
}
add_action( 'wp_head', 'focux_customize_css');

function focux_customize_js(){
	if ( get_theme_mod( 'custom_js')<>'' ){
		echo '<script type="text/javascript">'.
		get_theme_mod('custom_js')
		.'</script>';
	}
	
	echo get_theme_mod('footer_code').PHP_EOL;
}
add_action( 'wp_footer', 'focux_customize_js',20);

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function focux_customize_preview_js() {
	wp_enqueue_script( 'focux_customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), rand(), true );
}
add_action( 'customize_preview_init', 'focux_customize_preview_js' );
?>