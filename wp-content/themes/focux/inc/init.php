<?php
/**
 * focux Initialize
 *
 * @package focux
 */


$theme 	= wp_get_theme( 'focux' );

if ( ! function_exists( 'focux_support' ) ) :
function focux_support() {
	global $pagenow;
	if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) {
		wp_redirect(admin_url("themes.php?page=focux-welcome")); // Your admin page URL
		
	}

    /*
 	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on focux, use a find and replace
	 * to change 'focux' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'focux', get_template_directory() . '/languages' );
	$locale = get_locale(); 
	$locale_file = get_template_directory_uri()."/languages/$locale.php"; 
	if ( is_readable($locale_file) ) require_once($locale_file);

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	
	/* Enable the editor style */
	add_editor_style('editor-style.css');
	
	/* Declare WooCommerce support*/
	add_theme_support( 'woocommerce' );

	/*Change excerpt more string*/
	function focux_excerpt_more( $more ) {
		return '...';
	}
	add_filter( 'excerpt_more', 'focux_excerpt_more' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'focux' ),
		'mobile' => esc_html__( 'Mobile Menu', 'focux' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	
	// Set up the WordPress core custom header feature.
	add_theme_support( 'custom-header', apply_filters( 'focux_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => 'ffffff',
		'width'                  => 1200,
		'height'                 => 800,
		'flex-height'            => true
	) ) );

}

endif; // focux_setup

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function focux_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'focux_content_width', 1000 );
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function focux_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'focux' ),
		'id'            => 'sidebar',
		'description'   => esc_html__('This sidebar will included in the category and single post page.','focux'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Page Sidebar', 'focux' ),
		'id'            => 'sidebar-page',
		'description'   => esc_html__('This sidebar will included in the default page template.','focux'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	if ( class_exists( 'woocommerce' ) ){
	 register_sidebar( array(
		'name'          => esc_html__( 'Shop Sidebar', 'focux' ),
		'id'            => 'sidebar-shop',
		'description'   => esc_html__('This sidebar will included in the shop pages.','focux'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	 ) );
	}
	
	register_sidebar( array(
		'name'          => esc_html__( 'Bottom Widget 1', 'focux' ),
		'id'            => 'bottom-widget-1',
		'description'   => esc_html__('This widget area is placed at the left of bottom area.','focux'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
    ) );
    
    register_sidebar( array(
		'name'          => esc_html__( 'Bottom Widget 2', 'focux' ),
		'id'            => 'bottom-widget-2',
		'description'   => esc_html__('This widget area is placed at the second left of bottom area.','focux'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
    ) );
    
    register_sidebar( array(
		'name'          => esc_html__( 'Bottom Widget 3', 'focux' ),
		'id'            => 'bottom-widget-3',
		'description'   => esc_html__('This widget area is placed at the middle of bottom area.','focux'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
    ) );
    
    register_sidebar( array(
		'name'          => esc_html__( 'Bottom Widget 4', 'focux' ),
		'id'            => 'bottom-widget-4',
		'description'   => esc_html__('This widget area is placed at the right of bottom area.','focux'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
    ) );
}


/**
 * Load Google Fonts
 * Hook: focux_scripts
 */
function focux_font_styles() {
    wp_enqueue_style( 'focux-default-fonts', focux_fonts_url(), array(), null );
}
function focux_fonts_url() {
    $fonts_url = '';
    $karla = _x( 'on', 'Karla font: on or off', 'focux' );
    $open_sans = _x( 'on', 'Open Sans: on or off', 'focux' );
    $Lato = _x( 'on', 'Lato: on or off', 'focux' );
    
    if ('off' !== $karla || 'off' !== $open_sans || 'off' !== $Lato) {
	    $font_families = array();
	 
	    if ( 'off' !== $open_sans ) {
	        $font_families[] = 'Open Sans:100,300,400,400italic,500,500italic,700,700italic,900';
	    }
	    
	    if ( 'off' !== $karla ) {
	        $font_families[] = 'Karla:400,700';
	    }
	    
	    if ( 'off' !== $Lato ) {
	        $font_families[] = 'Lato:100,300,400,700 900';
	    }
	    
	    $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext,vietnamese,cyrillic-ext,cyrillic,greek,greek-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}
    
    return esc_url_raw( $fonts_url );
}

/**
 * Enqueue scripts and styles.
 * Hook: focux_scripts
 */
if ( ! function_exists( 'focux_scripts' ) ) :
function focux_scripts() {

	wp_enqueue_style( 'focux-style', get_stylesheet_uri(), '', null );
	wp_enqueue_style( 'focux-grid', get_template_directory_uri() .'/inc/css/grid.css', '', null );
	
	wp_enqueue_style( 'focux-main', get_template_directory_uri() .'/assets/css/main.css', '', null );
	
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() .'/inc/css/font-awesome/css/font-awesome.min.css', '', null );
	
	wp_enqueue_style( 'swiper', get_template_directory_uri() . '/inc/js/swiper/swiper.min.css', '',null);

	if ( class_exists( 'woocommerce' ) ){
	wp_enqueue_style( 'focux-woocommerce', get_template_directory_uri() .'/assets/css/woocommerce.css', '', null);
	}
	
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/inc/js/modernizr.custom.41385.js', array('jquery'), null, false );
	wp_enqueue_script( 'jquery-ease', get_template_directory_uri() . '/inc/js/jquery.easing.min.js', array('jquery'), null, false );
	wp_enqueue_script( 'focux-pushy', get_template_directory_uri() . '/inc/js/pushy.js', array('jquery'), null, true );
	wp_enqueue_script( 'scrollTo', get_template_directory_uri() . '/inc/js/jquery.scrollTo.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'localScroll', get_template_directory_uri() . '/inc/js/jquery.localScroll.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'jquery-swiper', get_template_directory_uri() . '/inc/js/swiper/swiper.jquery.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'imagesLoaded', get_template_directory_uri() . '/inc/js/jquery.imagesLoaded.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'masonry');
	wp_enqueue_script( 'focux-script', get_template_directory_uri() . '/assets/js/theme.js', array('jquery'),null, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
endif;


/* Load focux Framework
 * Include the framework function files.
 * Hook: focux_init
 */
if ( ! function_exists( 'focux_load_framework' ) ) :
function focux_load_framework(){
	
	/**
	 * Function liburary.
	 */
	require get_template_directory() . '/inc/functions/lib.php';
	
	/**
	 * Custom Widget.
	 */
	require get_template_directory() . '/inc/widgets/focux-recent-posts.php';
	require get_template_directory() . '/inc/widgets/focux-recent-comments.php';
	
	/**
	 * Customizer additions.
	 */
	require get_template_directory() . '/inc/customizer/customizer.php';
		
	/**
	 * Custom template tags for this theme.
	 */
	require get_template_directory() . '/inc/functions/template-tags.php';
	
	/**
	 * Custom functions that act independently of the theme templates.
	 */
	require get_template_directory() . '/inc/functions/extras.php';
	
	/**
	 * Include Welcome screen
	 */
	require get_template_directory() . '/inc/functions/welcome/welcome.php';

	
	/**
	 * If WooCommerce is actived, load custom woocommerce template tags for this theme.
	 */
	if ( class_exists( 'woocommerce' ) ){
		require get_template_directory() . '/inc/woocommerce/template-tags.php';
		require get_template_directory() . '/inc/woocommerce/hooks.php';
		require get_template_directory() . '/inc/woocommerce/extras.php';
		require get_template_directory() . '/inc/woocommerce/taxonomy_metabox.php';
	}
	
	/**
	 * Plugins
	 */
	require get_template_directory() . '/inc/plugins/plugins.php';
    
    /**
	 * If CMB2 is actived, load custom metabox
	 */
	require get_template_directory() . '/inc/functions/metabox.php';

	/**
	 * Demo Importer
	 */
	require get_template_directory() . '/inc/demo/demo-importer.php';

}
endif;

/**
 * Load Scripts in WP admin	
 */
function focux_admin_init(){
	wp_enqueue_style( 'farbtastic' );
	wp_enqueue_script( 'farbtastic' );
	wp_enqueue_style("focux-admin", get_template_directory_uri()."/inc/css/admin.css", false, "1.0", "all");
	wp_enqueue_script("admin_script", get_template_directory_uri()."/inc/js/admin_script.js",array('jquery'));
}

/**
 * Hooks
 */
require get_template_directory() . '/inc/functions/hooks.php';

do_action('focux_init');