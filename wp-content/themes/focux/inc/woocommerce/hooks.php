<?php
/**
 * Focux WooCommerce Hooks
 * * @package Focux
 */

/**
 * Wrapper
 * @hooked  focux_before_content()
 * @hooked  focux_after_content()
 * @hooked  focux_before_main_content()
 * @hooked  focux_after_main_content()
 * @hooked  focux_shop_sidebar()
 * @hooked  focux_before_shop
 * @hooked  focux_after_shop
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper',10 );
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
remove_action( 'woocommerce_after_main_content','woocommerce_output_content_wrapper_end', 10 );
remove_action( 'woocommerce_sidebar','woocommerce_get_sidebar', 10 );
add_action( 'woocommerce_before_main_content', 'focux_before_content',10); //<div class="hfeed site fx-grid-1000">
add_action( 'woocommerce_before_shop_loop','focux_before_shop',10 ); //<div class="category_result">
add_action( 'woocommerce_before_shop_loop','focux_after_shop',40 );  //</div>
add_action( 'woocommerce_before_shop_loop', 'focux_shop_sidebar',50); //Enable Sidebar
add_action( 'woocommerce_before_shop_loop', 'focux_before_shop_content',60);//<main>
add_action( 'woocommerce_after_shop_loop', 'focux_after_shop_content' ,10); // </main>
add_action( 'woocommerce_after_main_content', 'focux_after_content',30);//</div>


/**
 * Call Products
 * @see  focux_product_categories()
 * @see  focux_featured_products()
 * @see  focux_popular_products()
 * @see  focux_recent_products()
 * @see  focux_on_sale_products()
 */
add_action( 'focux_product_categories', 'focux_product_categories');
add_action( 'focux_featured_products', 'focux_featured_products');
add_action( 'focux_popular_products', 'focux_popular_products');
add_action( 'focux_recent_products', 'focux_recent_products');
add_action( 'focux_on_sale_products', 'focux_on_sale_products');

/**
 * Product Tabs
 * @hooked  focux_product_tab()
 */
remove_action( 'woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs',10 );
add_action( 'woocommerce_after_single_product_summary','focux_product_accordion',10 );

/*Remove the product tab heading*/
add_filter( 'woocommerce_product_description_heading', 'remove_product_tab_heading' );
add_filter('woocommerce_product_additional_information_heading', 'remove_product_tab_heading');
function remove_product_tab_heading() {
    return '';
} 

/**
 * Cart page wrapper
 * @hooked  focux_before_cart_table()
 * @hooked  focux_after_cart_table()
 */
add_action( 'woocommerce_before_cart_table','focux_before_cart_table',10 );
add_action( 'woocommerce_after_cart_table','focux_after_cart_table',10 );

/**
 * Move the cross sell display below the cart table
 */
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
add_action('woocommerce_after_cart','woocommerce_cross_sell_display');

/**
 * Custom cart button	
 * @hooked focux_add_to_cart()
 */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 ); 
add_action( 'woocommerce_before_shop_loop_item_title','focux_add_to_cart',10);


/**
 * Add Product category menu	
 * @hooked focux_woocommerce_category_menu()
 */
add_action( 'woocommerce_archive_description','focux_woocommerce_category_menu',10 );

/**
 * Product Loop
 * @hooked focux_template_loop_product_thumbnail()
 */
remove_action('woocommerce_before_shop_loop_item','woocommerce_template_loop_product_link_open',10);
remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close',5);
remove_action( 'woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail',10 );
remove_action( 'woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash',10);
add_action( 'woocommerce_before_shop_loop_item_title','focux_template_loop_product_thumbnail',10 );
remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title' );
add_action( 'woocommerce_shop_loop_item_title','focux_template_loop_product_title');

/**
 * Product Category Header Image
 * @hooked focux_woocommerce_category_image()
 */
add_action( 'woocommerce_archive_description', 'focux_woocommerce_category_slider', 20 );