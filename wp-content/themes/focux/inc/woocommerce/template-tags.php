<?php
/**
 * Custom WooCommerce template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 * Inspired by Storefront theme. * @package Focux
 */

if ( ! function_exists( 'focux_product_categories' ) ) {
	/**
	 * Display Product Categories
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function focux_product_categories( $args ) {

		if ( class_exists( 'woocommerce' ) ){

			$args = apply_filters( 'focux_product_categories_args', array(
				'limit' 			=> 3,
				'columns' 			=> 3,
				'child_categories' 	=> 0,
				'orderby' 			=> 'name',
				'title'				=> esc_html__( 'Product Categories', 'focux' ),
				) );

			echo '<section class="fx-product-section fx-product-categories">';

				do_action( 'focux_homepage_before_product_categories' );

				echo do_shortcode( '[product_categories number='.intval( $args['limit'] ).' columns='.intval( $args['columns'] ).' orderby='.esc_attr( $args['orderby'] ).' parent='.esc_attr( $args['child_categories'] ).']');

				do_action( 'focux_homepage_after_product_categories' );

			echo '</section>';

		}
	}
}

if ( ! function_exists( 'focux_recent_products' ) ) {
	/**
	 * Display Recent Products
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function focux_recent_products( $args ) {

		if ( class_exists( 'woocommerce' ) ){

			$args = apply_filters( 'focux_recent_products_args', array(
				'limit' 			=> 8,
				'columns' 			=> 4,
				'title'				=> esc_html__( 'Recent Products', 'focux' ),
				) );

			echo '<section class="fx-product-section fx-recent-products">';

				do_action( 'focux_homepage_before_recent_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '<div class="divider"><span></span></div></h2>';

				echo do_shortcode( '[recent_products per_page='.intval( $args['limit'] ).' columns='.intval( $args['columns'] ).']');
				do_action( 'focux_homepage_after_recent_products' );

			echo '</section>';
		}
	}
}

if ( ! function_exists( 'focux_featured_products' ) ) {
	/**
	 * Display Featured Products
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function focux_featured_products( $args ) {

		if ( class_exists( 'woocommerce' ) ){

			$args = apply_filters( 'focux_featured_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 2,
				'orderby'			=> 'date',
				'order'				=> 'desc',
				'title'				=> esc_html__( 'Handpicked Stuffs', 'focux' ),
				) );

			echo '<section class="fx-product-section fx-featured-products">';

				do_action( 'focux_homepage_before_featured_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '<div class="divider"><span></span></div></h2>';

				echo do_shortcode( '[featured_products per_page='.intval( $args['limit'] ).' columns='.intval( $args['columns'] ).' orderby='.esc_attr( $args['orderby'] ).' order='.esc_attr( $args['order'] ).']');
				
				do_action( 'focux_homepage_after_featured_products' );

			echo '</section>';

		}
	}
}

if ( ! function_exists( 'focux_popular_products' ) ) {
	/**
	 * Display Popular Products
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function focux_popular_products( $args ) {

		if ( class_exists( 'woocommerce' ) ){

			$args = apply_filters( 'focux_popular_products_args', array(
				'limit' 			=> 3,
				'columns' 			=> 3,
				'title'				=> esc_html__( 'Top Rated Products', 'focux' ),
				) );

			echo '<section class="fx-product-section fx-popular-products">';

				do_action( 'focux_homepage_before_popular_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '<div class="divider"><span></span></div></h2>';

				echo do_shortcode( '[top_rated_products per_page='.intval( $args['limit'] ).' columns='.intval( $args['columns'] ).']');
				do_action( 'focux_homepage_after_popular_products' );

			echo '</section>';

		}
	}
}

if ( ! function_exists( 'focux_on_sale_products' ) ) {
	/**
	 * Display On Sale Products
	 * Hooked into the `homepage` action in the homepage template
	 * @since  1.0.0
	 * @return void
	 */
	function focux_on_sale_products( $args ) {

		if ( class_exists( 'woocommerce' ) ){

			$args = apply_filters( 'focux_on_sale_products_args', array(
				'limit' 			=> 8,
				'columns' 			=> 4,
				'title'				=> esc_html__( 'On Sale', 'focux' ),
				) );

			echo '<section class="fx-product-section fx-on-sale-products">';

				do_action( 'focux_homepage_before_on_sale_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '<div class="divider"><span></span></div></h2>';

				echo do_shortcode( '[sale_products per_page='.intval( $args['limit'] ).' columns='.intval( $args['columns'] ).']');

				do_action( 'focux_homepage_after_on_sale_products' );

			echo '</section>';

		}
	}
}

if ( ! function_exists( 'focux_before_shop_content' ) ) :
/**
 * Before Main Content
 */
function focux_before_shop_content(){
       if ( is_product_category() ){
	      global $wp_query;
	      $cat = $wp_query->get_queried_object();
	      $cat_id = $cat->term_id;
          $cat_data = get_option("taxonomy_$cat_id");
          $cat_sidebar=$cat_data['cat_sidebar'];
          if($cat_sidebar=='yes'){
	       echo '<main id="fx-shop-primary" class="fx-grid fx-col9 last site-main">';
	      }
	    }
	    if(is_shop()){
	      $showSidebar = esc_html(get_theme_mod( 'shop_sidebar'));
	      if($showSidebar=='yes'){
	       echo '<main id="fx-shop-primary" class="fx-grid fx-col9 last site-main">';
	      }
	    }
}
endif;

if ( ! function_exists( 'focux_after_shop_content' ) ) :
/**
 * After Main Content
 */
function focux_after_shop_content(){
  if ( is_product_category() ){
	      global $wp_query;
	      $cat = $wp_query->get_queried_object();
	      $cat_id = $cat->term_id;
          $cat_data = get_option("taxonomy_$cat_id");
          $cat_sidebar=$cat_data['cat_sidebar'];
          if($cat_sidebar=='yes'){
	       	echo '</main>';
	      }
	    }
	    if(is_shop()){
	      $showSidebar = esc_html(get_theme_mod( 'shop_sidebar'));
	      if($showSidebar=='yes'){
	       	echo '</main>';
	      }
	    }
}
endif;

/**
 * Shop Sidebar
 * --------------------------------------------------
 */
if ( ! function_exists( 'focux_shop_sidebar' ) ) {
	function focux_shop_sidebar(){
	    if ( is_product_category() ){
	      global $wp_query;
	      $cat = $wp_query->get_queried_object();
	      $cat_id = $cat->term_id;
          $cat_data = get_option("taxonomy_$cat_id");
          $cat_sidebar=$cat_data['cat_sidebar'];
          if($cat_sidebar=='yes'){
	       get_sidebar('shop');
	      }
	    }
	    if(is_shop()){
	      $showSidebar = esc_html(get_theme_mod( 'shop_sidebar' ));
	      if($showSidebar=='yes'){
	       get_sidebar('shop');
	      }
	    }
	}
}

/**
 * Add Wrapper for the category result info
 * --------------------------------------------------
 */
if ( ! function_exists( 'focux_before_shop' ) ) {
	function focux_before_shop(){
	  $show=TRUE;
	  if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'subcategories') ){
	     $show=FALSE;
	  }
	  if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'subcategories') ){
	     $show = FALSE;
	  }
	  
	  if($show==TRUE){
		echo '<div class="category_result">';
	  }
	}
}
if ( ! function_exists( 'focux_after_shop' ) ) {
	function focux_after_shop(){
	  $show=TRUE;
	  if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'subcategories') ){
	     $show=FALSE;
	  }
	  if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'subcategories') ){
	     $show = FALSE;
	  }
	  
	  if($show==TRUE){
		echo '</div>';
	  }
	}
}

/**
 * Customize the product tab section
 * --------------------------------------------------
 */
function focux_product_accordion(){
	$tabs = apply_filters( 'woocommerce_product_tabs', array() );
    if ( ! empty( $tabs ) ) : 
?>
<?php    
    $i=0;
    $active=" active";
    $button='<span>&mdash;</span>';
    $closeWrapper='    </div>
                     </div>
                   </div>';
                   
    $startWrapper='<div id="fx-product-rest" class="hfeed site fx-grid-1000">
                    <div class="site-content">
                     <div>';
    $grid=' fx-grid-1000';
    
    foreach ( $tabs as $key => $tab ) : 
     if($i>0){
	    $active="";
	    $button='<span>+</span>';
	    $closeWrapper='';
	    $startWrapper='';
	    $grid='';
     }
     
     echo $closeWrapper;
?>    
	<div class="fx-accordion fx-product-content">
	    <div class="fx-accordion-section">
	        <a class="fx-accordion-section-title <?php echo esc_attr($grid).esc_attr($active);?>" href="#fx-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . esc_attr($key) . '_tab_title', esc_html( $tab['title'] ), $key ); ?><?php echo $button;?></a>
	        <div id="<?php echo 'fx-'.esc_attr( $key ); ?>" class="fx-accordion-section-content <?php echo esc_attr($active);?>">
	           <?php call_user_func( $tab['callback'], $key, $tab ); ?>
	        </div><!--end .accordion-section-content-->
	    </div><!--end .accordion-section-->
	</div><!--end .accordion-->
	<?php 
	 $i++; 
	 echo $startWrapper;
	 endforeach; 
	?>

<?php
endif;
}

/**
 * Customize Cart Page Wrapper
 * --------------------------------------------------
 */
function focux_before_cart_table(){
	echo '<div class="fx-grid fx-col8">';
}
function focux_after_cart_table(){
	echo '</div>';
}

/**
 * Custom add to cart button	
 */
function focux_add_to_cart(){
	global $product;
	$product_type = $product->product_type;
	$add_to_cart='';
	$cart_class='fx-add-to-cart-button';
	
	switch ( $product_type ) {
		case 'external':
			$add_to_cart='<i class="fa fa-external-link"></i>';
		break;
		case 'grouped':
			$add_to_cart='<i class="fa fa-plus"></i>';
		break;
		case 'simple':
			$add_to_cart='<i class="fa fa-shopping-cart"></i>';
			$cart_class .= ' button product_type_simple add_to_cart_button ajax_add_to_cart';
		break;
		case 'variable':
			$add_to_cart='<i class="fa fa-cog"></i>';
		break;
		default:
			$add_to_cart='<i class="fa fa-plus"></i>';
	}
	
    echo apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( isset( $quantity ) ? $quantity : 1 ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		esc_attr( isset( $class ) ? $class : $cart_class ),
		$add_to_cart
	),
    $product );
}

/**
 * Get the product thumbnail for the loop.	
 */
function focux_template_loop_product_thumbnail(){
	  global $post, $product, $woocommerce;
	  $hover_image='';
	  $onsale='';
	  
	  $attachment_ids = $product->get_gallery_attachment_ids();
	  if(count($attachment_ids)>0){
		  $attachment_id=$attachment_ids[0];
		  $hover_image='<span class="product_hover_image" style="background:url('.esc_url(wp_get_attachment_url( $attachment_id)).');background-size:cover;"></span>';
	  }
	  if ( $product->is_on_sale() ){
	      $onsale=apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'focux' ) . '</span>', $post, $product );
      }
	 
	  echo '<a href="'.esc_url(get_permalink()).'">'.$onsale.$hover_image.woocommerce_get_product_thumbnail().'</a>';
}

/**
 * Show the product title in the product loop. 
 */
function focux_template_loop_product_title() {
    echo '<h3><a href="'.esc_url(get_permalink()).'">' . esc_attr(get_the_title()) . '</a></h3>';
}

/**
 * Show the revolution slider in product category page. 
 */
function focux_woocommerce_category_slider() {

    if ( is_product_category() ){
	    global $wp_query;
	    $cat = $wp_query->get_queried_object();
	    $cat_id = $cat->term_id;
        $cat_data = get_option("taxonomy_$cat_id");
        $cat_slider=$cat_data['revslider_alias'];
	    if ($cat_slider<>'' ) {
	        echo '<div class="term-banner">';
		    putRevSlider(esc_attr($cat_slider));
		    echo '</div>';
		}
	}
}

/**
 * Show the product category menu. 
 */
function focux_woocommerce_category_menu() {

    // Find the category + category parent, if applicable
    $term 			= get_queried_object();
    $parent_id 		= empty( $term->term_id ) ? 0 : $term->term_id;
    $categories 	= get_terms('product_cat', array('hide_empty' => 1, 'parent' => $parent_id));

    $show_category_menu = TRUE;

    if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'subcategories') ) $show_category_menu = FALSE;
     if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'both') ) $show_category_menu = FALSE;
    
    if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'subcategories') ) $show_category_menu = FALSE;
    if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'both') ) $show_category_menu = FALSE;

	if ( is_product_category() && (get_woocommerce_term_meta($parent_id, 'display_type', true) == 'subcategories') ) $show_category_menu = FALSE;
    if ( is_product_category() && (get_woocommerce_term_meta($parent_id, 'display_type', true) == 'both') ) $show_category_menu = FALSE;
    
    if ( isset($_GET["s"]) && $_GET["s"] != '' ) $show_category_menu = FALSE;

    if ($show_category_menu == TRUE){
      if ($categories){
        
         echo '<ul class="fx_product_category_menu">';
         $cat_counter = 0; 
         foreach($categories as $category) :
                   
            echo'<li class="category_item">
                <a href="'.esc_url(get_term_link( $category->slug, 'product_cat' )).'" class="category_item_link">
                    <span class="category_name">'.esc_attr($category->name).'</span>
                </a>
            </li>';
               
          endforeach;
               
          echo '</ul><!--//product_categories-->';
        
       }
    }
}