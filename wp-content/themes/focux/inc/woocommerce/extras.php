<?php
/**
 * Custom functions that act independently of the WooCommerce default templates.
 * Eventually, some of the functionality here could be replaced by core features. * @package Focux
 */


/* Remove each style one by one
 * ----------------------------
 * All WooCommerce CSS file are combined in /framework/assets/css/woocommerce.css
 */
//add_filter( 'woocommerce_enqueue_styles', '__return_false' );

/**
 * Change 2 columns layout for WooCommerce Related Product
 * --------------------------------------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */ 
add_filter( 'woocommerce_output_related_products_args', 'focux_related_products_args' );
function focux_related_products_args( $args ) {
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 4; // arranged in 4 columns
	return $args;
}

/**
 * Customize the WooCommerce breadcrumb.
 * --------------------------------------------------------
 */ 
add_filter( 'woocommerce_breadcrumb_defaults', 'focux_woocommerce_breadcrumbs' );
function focux_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' &#47; ',
            'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb"><i class="fa fa-home"></i>',
            'wrap_after'  => '</nav>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

/**
 * Products per page
 * ---------------------------------------------------
 */
add_filter( 'loop_shop_per_page','focux_products_per_page' );
function focux_products_per_page() {
	return intval( apply_filters( 'focux_products_per_page', 12 ) );
}

/**
 * Change Cross Product to 4 columns
 */
add_filter( 'woocommerce_cross_sell_display_args', 'focux_cross_sell_display_args' );
function focux_cross_sell_display_args( $args ) {
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 4; // arranged in 4 columns
	return $args;
}

/**
 * Customize the woocommerce breadcrumb.
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'focux_change_breadcrumb_delimiter' );
function focux_change_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
	$defaults['delimiter'] = '<span class="delimiter">&raquo;</span>';
	$defaults['wrap_before'] = '<div itemscope class="focux-breadcrumbs"><div class="fx-grid-1000"><i class="fa fa-home"></i>';
	$defaults['wrap_after'] = '</div></div>';
	return $defaults;
}
?>