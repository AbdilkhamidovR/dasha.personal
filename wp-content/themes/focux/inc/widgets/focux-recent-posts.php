<?php
/**
 * Recent Posts Widgets
 * @package focux Framework
 * @subpackage City News
 * @since 1.0
 */
class focux_recent_posts extends WP_Widget {


	/**
	 * Register widget
	**/
	public function __construct() {
		
		parent::__construct(
	 		'focux_recent_posts', // Base ID
			esc_html__( '(Focux) Recent Posts with thumbnail', 'focux' ), // Name
			array( 'description' => esc_html__( 'Show the recent posts with thumbnail and excerpt', 'focux' ), ) // Args
		);

	}
	
   function widget($args, $instance) {
        extract( $args );
		$title = apply_filters('widget_title', $instance['title'] );
		$number = $instance['number'];
        $thumbnail= $instance['thumbnail'];
		$category= focux_cat_slug($instance['category']);
		
		echo $before_widget;
		echo $before_title;
		echo esc_attr($title);
		echo $after_title;
		
		echo focux_post_list($number,$thumbnail,$category);
		echo $after_widget;
   }
   
   function update($new_instance, $old_instance) {
	   $instance['category'] = strip_tags( $new_instance['category'] );
	   $instance['title'] = strip_tags( $new_instance['title'] );
       $instance['thumbnail'] = strip_tags( $new_instance['thumbnail'] );
	   $instance['number'] = strip_tags($new_instance['number']);                
       return $instance;
   }
   function form($instance) {  
	   $defaults = array( 'title' => esc_html__('Recent Blog', 'focux'),'number' => '5','thumbnail' => 'yes','category'=>'');
	   $instance = wp_parse_args( (array) $instance, $defaults ); 
		
	   $title = esc_attr($instance['title']);
       $thumbnail = esc_attr($instance['thumbnail']);
	   $category = esc_attr($instance['category']);
	   $number = esc_attr($instance['number']);
   ?>
    <p>
	<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Title', 'focux'); ?></label>
	<input type="text" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo $title; ?>" />
    </p>
   
    <p>
        <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php _e('Number','focux'); ?></label>
        <input type="text" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $number; ?>" id="<?php echo $this->get_field_id('number'); ?>" style="width:20%;" /> <span style="color:#999;">The Max number:20</span>
    </p>
    
     <p>
        <label for="<?php echo esc_attr($this->get_field_id('thumbnail')); ?>"><?php _e('Show thumbnail','focux'); ?></label>
        <select name="<?php echo esc_attr($this->get_field_name('thumbnail')); ?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('thumbnail')); ?>">
        <?php if($thumbnail == 'yes'){?>
           <option value="no">No</option>
        <?php }elseif($thumbnail=='no'){?>
           <option value="yes">Yes</option>
        <?php }?>
           <option value="<?php if($thumbnail == 'yes'){echo 'yes';}elseif($thumbnail=='no'){echo 'no';}?>" selected="selected"><?php if($thumbnail == 'yes'){echo'Yes';}elseif($thumbnail=='no'){echo'No';}?></option>
        </select>
    </p>
    
     <p>
            <label for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php _e('Category:','focux'); ?></label>
            <select name="<?php echo esc_attr($this->get_field_name('category')); ?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('category')); ?>">
                <?php if($cate_name==''):?>
                <option value="">All</option>
                <?php endif;?>
                <?php 
				$thecatlist_A = get_categories('hide_empty=0');
                $catdlist = array();
				foreach ($thecatlist_A as $catforlist){ 
				$cate_name = $catforlist->cat_name;
				?>
                <option value="<?php echo esc_attr($cate_name);?>" <?php if($category == $cate_name){ echo "selected='selected'";} ?>><?php echo esc_attr($cate_name); ?></option>
                <?php }?>
            </select>
     </p>
   <?php
   }
}
register_widget('focux_recent_posts');

/*Recent Posts Widget*/
if( !function_exists( 'focux_post_list') ){
	function focux_post_list($number=5,$thumbnail='yes',$category_slug=''){
	   global $post;
		$tmp_post = $post;
		if($category_slug<>''){
		  $category_array=explode(',',$category_slug);
		  $args = array( 
			'numberposts' => $number, 
			'orderby' => 'post_date',
			'order'=>'DESC',
			'tax_query' => array(
				array(
				  'taxonomy' => 'category',
				  'field' => 'slug',
				  'terms' => $category_array,
				  'include_children' => false
				)
			  ));
		}else{
		  $args = array( 
			'numberposts' => $number, 
			'orderby' => 'post_date',
			'order'=>'DESC'
		  );
		}
		$posts = get_posts($args);
		$date='';
		$post_list='';
		$post_list.='<ul class="post_list">';
		foreach($posts as $post){
		    setup_postdata($post);
			$url=esc_url(get_permalink($post->ID));
			$title=$post->post_title;	

			$post_list.='<li class="post-list-'.$post->ID.'">';
			if($thumbnail=='yes'){ 
				if(has_post_thumbnail($post->ID)){
					  $image_id = get_post_thumbnail_id($post->ID);
					  $thumbnail_url = wp_get_attachment_image_src($image_id, 'thumbnail', true);
					  $post_list.='<a href="'.esc_url($url).'" class="thumbnail"><img src="'.esc_url($thumbnail_url[0]).'" alt="'.esc_attr($title).'" /></a>';
				}
			$date='<p>'.get_the_time(get_option('date_format')).'</p>';
			}
			$post_list.='<a href="'.esc_url($url).'" class="post_title">'.wp_trim_words(esc_attr($title),20,'...').'</a>';
			$post_list.=$date;
			$post_list.='<div class="clear"></div></li>';
		 }
		$post_list.='</ul>';
		$post = $tmp_post;
	    return $post_list;
	}
}
?>