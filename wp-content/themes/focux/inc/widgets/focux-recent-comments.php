<?php
/**
 * Recent Comments Widgets
 * @package Focux
 * @since 1.0
 */

class focux_recent_comments extends WP_Widget {
	
	
	/**
	 * Register widget
	**/
	public function __construct() {
		
		parent::__construct(
	 		'focux_recent_comments', // Base ID
			esc_html__( '(Focux) Recent Comments', 'focux' ), // Name
			array( 'description' => esc_html__( 'Display the most latest comments with avatar ', 'focux' ), ) // Args
		);
		
	}

	
	/**
	 * Front-end display of widget
	**/
	public function widget( $args, $instance ) {
				
		extract( $args );

		$title = apply_filters('widget_title', isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : 'Latest Comments' );
		$comments_show = isset( $instance['comments_show'] ) ? esc_attr( $instance['comments_show'] ) : '5';
		

		echo $before_widget;
		if ( $title ) echo $before_title . $title . $after_title;
		echo focux_recent_comment($comments_show);
		echo $after_widget;
		
	}
	
	
	/**
	 * Sanitize widget form values as they are saved
	**/
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();

		/* Strip tags to remove HTML. For text inputs and textarea. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['comments_show'] = strip_tags( $new_instance['comments_show'] );
		
		return $instance;
		
	}
	
	
	/**
	 * Back-end widget form
	**/
	public function form( $instance ) {
		
		/* Default widget settings. */
		$defaults = array(
			'title' => 'Latest Comments',
			'comments_show' => '5',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Title:', 'focux'); ?></label>
			<input type="text" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'comments_show' )); ?>"><?php _e('Comments to show:', 'focux'); ?></label>
			<input type="text" id="<?php echo esc_attr($this->get_field_id( 'comments_show' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'comments_show' )); ?>" value="<?php echo esc_attr($instance['comments_show']); ?>" size="1" />
		</p>
		<p>
	<?php
	}

}
register_widget( 'focux_recent_comments' );

/*Recent Comment With Avatar*/
function focux_recent_comment($number){
    $commentNum = 1;
    
    // Get the comments
	$recent_comments = get_comments( array(
	  'number' => $number,
	  'status' => 'approve',
	  'type' => 'comment'
	) );
	$commentList='<ul>';
		foreach ($recent_comments as $comment){
		$commentList.='<li>
	             <header class="clearfix">
	                	<figure>
	                        <a href="'.esc_url(get_permalink( $comment->comment_post_ID )).'">
	                            '.get_avatar( $comment->comment_author_email, '40' ).'
	                        </a>
	                        <span class="comment-author-name">
	                        '.$comment->comment_author.'
	                        </span>
	                        <div class="comment-order">'.$commentNum.'</div>
	                    </figure>
	                    
	                    <a class="comment-post" href="'.esc_url(get_permalink( $comment->comment_post_ID )).'#comment-'.esc_attr($comment->comment_ID).'">
	                       '.esc_attr(wp_trim_words(get_the_title( $comment->comment_post_ID ),20,'...')).'
	                    </a>
	                </header>
	                
	                <div class="comment-text">
	                    <div class="up-arrow"></div>
	                	'.wp_trim_words( $comment->comment_content, 30,'...' ).'
	                </div>
				</li>';
		$commentNum++;
		}
				
		$commentList.='</ul>';
		
		return $commentList;
}
?>