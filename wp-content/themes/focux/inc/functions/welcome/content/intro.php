<?php
/**
 * Welcome screen intro template
 */
?>
<?php
$focux = wp_get_theme( 'focux' );
$allow_tags = array(
    //formatting
    'strong' => array(),
    'em'     => array(),
    'b'      => array(),
    'i'      => array(),

    //links
    'a'     => array(
        'href' => array()
    )
);
?>
<div class="col two-col" style="overflow: hidden;">
	<h1><?php echo '<strong>'.esc_html_e('Focux','focux').'</strong> <sup class="version">' . esc_attr( $focux['Version'] ) . '</sup>'; ?></h1>
	
	<div class="title-right"><a href="http://envato.themevan.com/forum/focux/" target="_blank" class="button"><?php esc_html_e('Free Support','focux');?></a> <a href="http://www.themevan.com/contact" target="_blank" class="button button-primary"><?php esc_html_e('Need Customization Service?','focux');?></a></div>


	<div class="col boxed enrich"><h2>
<?php esc_html_e('Build a responsive eCommerce website for your business.', 'focux'); ?></h2>

		<p><?php esc_html_e( 'Focux is a multi-purpose eCommerce WordPress theme based on WooCommerce. We enhanced the description content from plain text to rich content with different elements and layouts for each single product post.', 'focux'); ?></p>
		
        
        <h3><?php esc_html_e( 'Only 3 Steps For Focux Theme Setup', 'focux'); ?></h3>
        <ol>
			<li><?php printf(wp_kses(__('Please activate all the required plugins first through <a href="%s" target="_blank">"Appearance > Install Plugins"</a>','focux'),$allow_tags),esc_url(home_url('/')).'wp-admin/themes.php?page=tgmpa-install-plugins');?></li>
		
		<li>
		<?php printf(wp_kses(__('You can one click import all demo content through <a href="%s" target="_blank">Appearance > Import Demo Data</a>.','focux'),$allow_tags), esc_url(home_url('/')).'wp-admin//themes.php?page=pt-one-click-demo-import');?>
		</li>
		
		 
		 <li>		 		 
		 <?php printf(wp_kses(__('(Additionally) There are some custom options included in the <a href="%s" target="_blank">Appearance > Customize</a> that allow you to change the font and color scheme or manage some other features.
','focux'),$allow_tags), esc_url(home_url('/')).'wp-admin/customize.php');?>
		 </li>
        </ol>

        <h2><?php esc_html_e( 'Documentations', 'focux' ); ?></h2>
		<ul>
		  <li><a href="http://themevan.com/docs/focux" target="_blank">- <?php esc_html_e('Focux Documentation','focux');?></a></li>
		  <li><a href="https://wpbakery.atlassian.net/wiki/display/VC/Visual+Composer+Pagebuilder+for+WordPress" target="_blank">- <?php esc_html_e('Visual Composer Documentation','focux');?></a></li>
		  <li><a href="https://www.themepunch.com/revslider-doc/slider-revolution-documentation/" target="_blank">- <?php esc_html_e('Revolution Slider Documentation','focux');?></a></li>

		</ul>
	</div>

	<div class="col boxed faq">
		<h2><?php esc_html_e( 'FREQUENTLY ASKED QUESTIONS', 'focux' ); ?></h2>
		<div class="questions">
		    <div class="item">
			  <h4><?php esc_html_e("1. Why I can't import the sample data?",'focux');?></h4>
			  <p><?php esc_html_e("You'd better deactivated all plugins except WooCommerce before you import the sample data. After the data is imported, then reactivate the plugins.","focux");?></p>
			</div>
			
			<div class="item">
			  <h4><?php esc_html_e("2. How I upgrade the bundled plugins?",'focux');?></h4>
			  <p><?php esc_html_e("Because you can't upgrade the bundled plugins automatically in the WP backend, so you have to upgrade them manually. We always include the latest version of these bundled plugins in the newest version of Focux. After you reinstall the new version of Focux theme, just remove those old version of bundled plugins, and then you will see the notification bar appeared on the top, it reminds you to install the required plugins, just click install link and activate them.","focux");?></p>
			</div>
		
			<div class="item">
			  <h4><?php esc_html_e("3. What's the safe way to customize the theme without lose my changes when I upgrade the theme in the future?",'focux');?></h4>
			  <p><?php esc_html_e("First of all, We strongly suggest you don't modify the Focux theme, otherwise, you will not able to upgrade the theme smoothly in the future. ",'focux');?> </p>
			  <p><?php printf(wp_kses(__("The best way is use child theme, you can put all your custom changes into child theme folder. Learn more about the <a href='%s' target='_blank'>Child Theme in WordPress Codex</a>. ",'focux'),$allow_tags),'https://codex.wordpress.org/Child_Themes');?></p>
			  
			  <p>
			  <?php printf(wp_kses(__("If you don't want to create child theme, you can put the custom CSS and JS into 'Appearance > Customizer > Custom Code', or install <a href='%s' target='_blank'>Custom css-js-php plugin</a>, it allows you to insert your own custom CSS, JS and PHP functions through your WordPress backend.  ",'focux'),$allow_tags),'https://wordpress.org/plugins/custom-css-js-php/');?>
			  </p>
			</div>
			
		</div>
		
		
		
	</div>
	
</div>