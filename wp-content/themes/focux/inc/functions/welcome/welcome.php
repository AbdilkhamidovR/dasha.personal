<?php
/**
 * Welcome Screen Class
 * Sets up the welcome screen page, hides the menu item
 * and contains the screen content.
 */
class focux_Welcome {

	/**
	 * Constructor
	 * Sets up the welcome screen
	 */
	public function __construct() {

		add_action( 'admin_menu', array( $this, 'focux_welcome_register_menu' ) );
		add_action( 'load-themes.php', array( $this, 'focux_activation_admin_notice' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'focux_welcome_style' ) );

		add_action( 'focux_welcome', array( $this, 'focux_welcome_intro' ), 				10 );

	} // end constructor

	/**
	 * Adds an admin notice upon successful activation.
	 * @since 1.0.0
	 */
	public function focux_activation_admin_notice() {
		global $pagenow;

		if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) { // input var okay
			add_action( 'admin_notices', array( $this, 'focux_welcome_admin_notice' ), 99 );
		}
	}

	/**
	 * Display an admin notice linking to the welcome screen
	 * @since 1.0.0
	 */
	public function focux_welcome_admin_notice() {
		?>
			<div class="updated notice is-dismissible">
				<p><?php echo sprintf( esc_html__( 'Thanks for choosing Focux! Just click the following button to get more useful tips on getting started step by step.', 'focux' ), '<a href="' . esc_url( admin_url( 'themes.php?page=focux-welcome' ) ) . '">', '</a>' ); ?></p>
				<p><a href="<?php echo esc_url( admin_url( 'themes.php?page=focux-welcome' ) ); ?>" class="button" style="text-decoration: none;"><?php _e( 'Get started with Focux', 'focux' ); ?></a></p>
			</div>
		<?php
	}

	/**
	 * Load welcome screen css
	 * @return void
	 * @since  1.0.0
	 */
	public function focux_welcome_style( $hook_suffix ) {
		global $focux_version;

		if ( 'appearance_page_focux-welcome' == $hook_suffix ) {
			wp_enqueue_style( 'focux-welcome', get_template_directory_uri() . '/inc/functions/welcome/css/welcome.css', $focux_version );
			wp_enqueue_style( 'thickbox' );
			wp_enqueue_script( 'thickbox' );
		}
	}

	/**
	 * Creates the dashboard page
	 * @see  add_theme_page()
	 * @since 1.0.0
	 */
	public function focux_welcome_register_menu() {
		add_theme_page( 'Focux Guide', 'Focux Guide', 'activate_plugins', 'focux-welcome', array( $this, 'focux_welcome_screen' ) );
	}

	/**
	 * The welcome screen
	 * @since 1.0.0
	 */
	public function focux_welcome_screen() {
		?>
		<div class="wrap about-wrap">

			<?php
			/**
			 * @hooked focux_welcome_intro - 10
			 */
			do_action( 'focux_welcome' ); ?>

		</div>
		<?php
	}

	/**
	 * Welcome screen intro
	 * @since 1.0.0
	 */
	public function focux_welcome_intro() {
		require_once( get_template_directory() . '/inc/functions/welcome/content/intro.php' );
	}

}

$GLOBALS['focux_Welcome'] = new focux_Welcome();