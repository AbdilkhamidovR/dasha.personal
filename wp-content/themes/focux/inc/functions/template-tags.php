<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 * * @package Focux
 */

if ( ! function_exists( 'focux_before_content' ) ) :
/**
 * Before Content
 */
function focux_before_content(){
	echo '<div id="page" class="hfeed site fx-grid-1000">
	  <div id="content" class="site-content">';
}
endif;

if ( ! function_exists( 'focux_after_content' ) ) :
/**
 * After Content
 * Closes the wrapping divs
 */
function focux_after_content(){
	echo '</div>
	   </div>';
}
endif;

if ( ! function_exists( 'focux_before_main_content' ) ) :
/**
 * Before Main Content
 */
function focux_before_main_content(){
	echo '<main id="primary" class="fx-grid fx-col9 site-main">';
}
endif;

if ( ! function_exists( 'focux_after_main_content' ) ) :
/**
 * After Main Content
 */
function focux_after_main_content(){
	echo '</main>';
}
endif;

if ( ! function_exists( 'focux_after_conent' ) ) :
/**
 * After Content
 * Closes the wrapping divs
 */
function focux_after_conent(){
	echo '</div>
	   </div>';
}
endif;


if ( ! function_exists( 'focux_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function focux_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'focux' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'focux' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on"><i class="fa fa-clock-o"></i> ' . $posted_on . '</span><span class="byline"><i class="fa fa-user"></i> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'focux_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function focux_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'focux' ) );
		if ( $categories_list && focux_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'focux' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'focux' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'focux' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'focux' ), esc_html__( '1 Comment', 'focux' ), esc_html__( '% Comments', 'focux' ) );
		echo '</span>';
	}

}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function focux_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'focux_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'focux_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so focux_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so focux_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in focux_categorized_blog.
 */
function focux_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'focux_categories' );
}
add_action( 'edit_category', 'focux_category_transient_flusher' );
add_action( 'save_post',     'focux_category_transient_flusher' );

/**
 * Custom LOGO and Text
 */
function focux_custom_logo(){
        $logo_url=esc_url(get_theme_mod( 'logo_upload'));
        $page_logo_url=focux_page_custom_logo();
        
        if($page_logo_url<>'' && isset($page_logo_url)){
          $logo_url=$page_logo_url;
        }
	    if (!empty($logo_url) && $logo_url<>'' ) {
			$custom_logo='<img id="site-logo" src="'.$logo_url.'" alt="'.esc_attr(get_bloginfo('name')).'" />';
	    } else { 
		   $custom_logo=esc_attr(get_bloginfo('name'));
		}
		
		echo '<div class="site-branding fx-grid fx-col3">';
			if ( is_front_page() && is_home() ){
				echo'<h1 class="site-title"><a href="'.esc_url( home_url( '/' ) ).'" rel="home">'.$custom_logo.'</a></h1>';
			}else{
				echo'<span class="site-title"><a href="'.esc_url( home_url( '/' ) ).'" rel="home">'.$custom_logo.'</a></span>';
			}
		echo '</div><!-- .site-branding -->';
}

/**
 * Primary Navigation
 */
function focux_primary_navigation(){
	        echo'<nav id="site-navigation" class="main-navigation fx-grid fx-col7">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars"></i></button>'
			.wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu','echo' => false ) ).
		   '</nav><!-- #site-navigation -->';
}

/**
 * Top Buttons in Navigation
 */
function focux_top_buttons(){
  if ( class_exists( 'woocommerce' ) ){
	   echo'<div id="site-icons" class="fx-grid fx-col3">
		  <a href="'.esc_url(home_url('/')).'?page_id='.get_option('woocommerce_myaccount_page_id').'"><i class="fa fa-user"></i></a>
		  <a href="javascript:void(0);" id="product_cart"><i class="fa fa-shopping-cart"></i></a>'; 
	      echo'<a href="javascript:void(0);" id="product_search"><i class="fa fa-search"></i></a>';
	   echo'</div>';
  }
}

function focux_cover(){
	if(is_home() || is_front_page()){
	  if(has_header_image()){
	 echo'<div id="fx-header-cover">
			  <h2>'.get_bloginfo('description').'</h2>
			  <a href="#page" title="'.esc_html__('Explore','focux').'" class="view-content"><i class="fa fa-angle-down"></i></a>
   	 </div>
   	 <div id="fx-header-overlay"></div>';
   	 }
    }
}

/**
 * The Wrapper Before Navigation
 */
function focux_before_navigation(){
	   echo'<div id="fx-topbar">';
	   echo'<div id="fx-primary-bar" class="fx-grid-1000">';
}

/**
 * The Wrapper After Navigation
 */
function focux_after_navigation(){
	   echo'</div>
	  </div>';
}

/**
 * Copyright Text in Footer	
 */
function focux_copyright(){
    $copyright=get_theme_mod( 'copyright');
	if(!empty($copyright) && $copyright<>''){
	    echo esc_attr(get_theme_mod( 'copyright'));
	}else{
        echo esc_html__( 'Proudly powered by', 'focux' ).'<a href="https://wordpress.org/" target="_blank"> WordPress</a><span class="sep"> | </span>'.esc_html__( 'Designed by','focux').' <a href="http://www.themevan.com" target="_blank" title="We Design Premium WordPress Themes">ThemeVan</a>';
	}
}

/**
 * Bullet links at the right side
 */
function focux_bullet_links(){
	 global $post;
	 if(is_page()):
		 while ( have_posts() ) : the_post(); 
	     $bullet_links = esc_url(get_post_meta( get_the_ID(), '_focux_custom_bullets_link', true ));
	     endwhile; // End of the loop. 

	    $bullet_links_array=explode(',',$bullet_links);
	    
	    if($bullet_links<>''){
		    echo '<div class="bullet_links">';
		    for($i=0;$i<count($bullet_links_array);$i++){
			    echo '<a class="anchor" href="'.$bullet_links_array[$i].'">'.$bullet_links_array[$i].'</a>';
		    }
		    echo '</div>';
	    }
    endif;
}

/**
 * Custom LOGO for each page
 */
function focux_page_custom_logo(){
	global $post;
	$logo='';
	while ( have_posts() ) : the_post(); 
     $logo = esc_url(get_post_meta( $post->ID, '_focux_custom_logo', true ));
    endwhile; // End of the loop. 
    
    return $logo;
}