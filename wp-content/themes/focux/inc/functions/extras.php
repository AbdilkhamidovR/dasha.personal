<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 * * @package Focux
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function focux_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	
	if ( class_exists( 'woocommerce') ){
	     $classes[]='woocommerce';
	}

	return $classes;
}
add_filter( 'body_class', 'focux_body_classes' );

/**
 * Remove query strings from static resources
 */
function focux_remove_script_version( $src ){
  $parsed = parse_url($src);

  if (isset($parsed['query'])) {
    parse_str($parsed['query'], $qrystr);
    if (isset($qrystr['ver'])) {
      unset($qrystr['ver']); 
    }
    
    $parsed['query'] = http_build_query($qrystr);
  }

  $src = '';
  $src .= (!empty($parsed['scheme'])) ? $parsed['scheme'].'://' : '';
  $src .= (!empty($parsed['host'])) ? $parsed['host'] : '';
  $src .= (!empty($parsed['path'])) ? $parsed['path'] : '';
  $src .= (!empty($parsed['query'])) ? '?'.$parsed['query'] : '';

  return $src;
}
add_filter( 'script_loader_src', 'focux_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'focux_remove_script_version', 15, 1 );

/**
 * Modify the default WordPress search form
 */

function focux_default_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform_default" class="searchform" action="' . esc_url(home_url( '/' )) . '" >
    <div><label class="screen-reader-text" for="s">' . esc_html__( 'Search for:','focux' ) . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="'. esc_attr__( 'Search','focux' ) .'" />
    <input type="hidden" name="post_type" id="post_type_default" value="post" />
    </div>
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'focux_default_search_form', 100 );


/*Add social profile fields to user page*/
function focux_custom_profile( $contactmethods ) {
    $contactmethods['facebook'] = 'Facebook';
	$contactmethods['twitter'] = 'Twitter';
	$contactmethods['google-plus'] = 'Google+';
	$contactmethods['flickr'] = 'Flickr+';
	$contactmethods['instagram'] = 'Instagram';
	$contactmethods['tumblr'] = 'Tumblr';
	$contactmethods['github'] = 'Github';
	$contactmethods['youtube'] = 'Youtube';
	$contactmethods['vimeo'] = 'Vimeo';
	$contactmethods['pinterest'] = 'Pinterest';
	$contactmethods['wordpress'] = 'WordPress';
    return $contactmethods;
}
add_filter('user_contactmethods','focux_custom_profile',10,1);

add_action('focux_author_social_profile','focux_author_socials');
function focux_author_socials(){
	$social_profile='';
	$social_array=array('facebook','twitter','google-plus','flickr','instagram','tumblr','github','youtube','vimeo','wordpress');
	for($i=0;$i<count($social_array);$i++){
	  $social_name=$social_array[$i];
	  if(get_the_author_meta($social_array[$i])<>''){
	    if($social_name=='vimeo'){
		    $social_name=$social_array[$i].'-square';
	    }
	    $social_profile.='<a href="'.esc_url(get_the_author_meta($social_array[$i])).'" target="_blank"><i class="fa fa-'.esc_attr($social_name).'"></i></a>';
	  }
	}
	echo $social_profile;
}

/*Narrow Layout*/
function focux_narrow_container($echo){
	if(get_theme_mod( 'archive_sidebar')=='no' || null == get_theme_mod( 'archive_sidebar') || get_theme_mod( 'post_sidebar')=='no' || null == get_theme_mod( 'post_sidebar')){
		return $echo;
	}
}

/*Custom Post Navigation*/
function focux_post_navigation( $args = array() ) {
    $args = wp_parse_args( $args, array(
        'prev_text'          => '%title',
        'next_text'          => '%title',
        'screen_reader_text' => esc_html__( 'Post navigation','focux' ),
    ) );

    $navigation = '';
    $previous   = get_previous_post_link( '<div class="nav-previous">%link</div>', $args['prev_text'] );
    $next       = get_next_post_link( '<div class="nav-next">%link</div>', $args['next_text'] );
    $class      = 'post-navigation'.focux_narrow_container('-narrow');
    
    // Only add markup if there's somewhere to navigate to.
    if ( $previous || $next ) {
        $navigation = _navigation_markup( $previous . $next, $class, $args['screen_reader_text'] );
    }

    echo $navigation;
}

			
/*Remove exist shortcode*/
if ( function_exists( 'vc_map')){
	vc_remove_element("vc_carousel");
	vc_remove_element("vc_posts_slider");
	vc_remove_element("product_page");
	vc_remove_element("product_attribute");
	vc_remove_element("woocommerce_checkout");
}

/*Remove Max Mega Menu Options*/
if ( function_exists('max_mega_menu_is_enabled') && max_mega_menu_is_enabled('primary') ){
	function focux_remove_maxmegamenu () {
		remove_menu_page( 'maxmegamenu' );
	}
	add_action('admin_menu', 'focux_remove_maxmegamenu');
}