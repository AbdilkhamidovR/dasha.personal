<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'focux_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */


/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 object $cmb CMB2 object
 *
 * @return bool             True if metabox should show
 */
function focux_show_if_front_page( $cmb ) {
	// Don't show this metabox if it's not the front page template
	if ( $cmb->object_id !== get_option( 'page_on_front' ) ) {
		return false;
	}
	return true;
}

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
function focux_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

/**
 * Conditionally displays a message if the $post_id is 2
 *
 * @param  array             $field_args Array of field parameters
 * @param  CMB2_Field object $field      Field object
 */
function focux_before_row_if_2( $field_args, $field ) {
	if ( 2 == $field->object_id ) {
		echo '<p>Testing <b>"before_row"</b> parameter (on $post_id 2)</p>';
	} else {
		echo '<p>Testing <b>"before_row"</b> parameter (<b>NOT</b> on $post_id 2)</p>';
	}
}

add_action( 'cmb2_admin_init', 'focux_register_shop_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function focux_register_shop_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_focux_custom_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb_focux = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Custom Page Settings', 'focux' ),
		'object_types'  => array( 'page'), // Post type
		'priority'   => 'high',
	) );

	
	$cmb_focux->add_field( array(
		'name'             => esc_html__( 'Page Custom LOGO', 'focux' ),
		'desc'             => esc_html__( 'The page custom LOGO will replace the default LOGO in this page. The recommended size of LOGO is 280x60px', 'focux' ),
		'id'               => $prefix . 'logo',
		'type'             => 'file',
	    'options' => array(
	        'url' => true, // Hide the text input for the url
	    ),
	    'text'    => array(
	        'add_upload_file_text' => esc_html__('Upload Custom LOGO','focux') // Change upload button text. Default: "Add or Upload File"
	    ),
	) );

	$cmb_focux->add_field( array(
		'name'             => esc_html__( 'Side Bullets Link', 'focux' ),
		'desc'             => esc_html__( 'Add the row id above and separate them by English comma, e.g. #about,#service,#contact', 'focux' ),
		'id'               => $prefix . 'bullets_link',
		'type'             => 'text'
	) );
	
	
}