<?php
/**
 * Focux Hooks
 * * @package Focux
 */

/**
 * General
 * @see  focux_support()
 * @see  focux_content_width()
 * @see  focux_widgets_init()
 * @see  focux_scripts()
 * @see  focux_font_styles()
 * @see  focux_load_framework()
 */
add_action( 'after_setup_theme', 'focux_support' );
add_action( 'after_setup_theme', 'focux_content_width', 0 );
add_action( 'widgets_init', 'focux_widgets_init' );
add_action( 'wp_enqueue_scripts', 'focux_scripts',20);
add_action( 'wp_enqueue_scripts', 'focux_font_styles' );
add_action( 'focux_init', 'focux_load_framework' );
add_action( 'admin_init', 'focux_admin_init');

/**
 * Wrapper
 * @see  focux_before_content()
 * @see  focux_after_content()
 * @see  focux_before_main_content()
 * @see  focux_after_main_content()
 */
add_action( 'focux_before_content', 'focux_before_content' );
add_action( 'focux_after_content', 'focux_after_content');
add_action( 'focux_before_main_content', 'focux_before_main_content' );
add_action( 'focux_after_main_content', 'focux_after_main_content' );


/**
 * Header
 * @see focux_before_navigation()
 * @see focux_custom_logo()
 * @see focux_primary_navigation()
 * @see focux_top_buttons()
 * @see focux_after_navigation()
 * @see focux_cover()	
 * @see focux_breadcrumbs
 */
add_action( 'focux_header', 'focux_before_navigation',0);
add_action( 'focux_header', 'focux_custom_logo',10);
add_action( 'focux_header', 'focux_primary_navigation',20);
add_action( 'focux_header', 'focux_top_buttons',30);
add_action( 'focux_header', 'focux_after_navigation',40);
add_action( 'focux_header', 'focux_cover',50);
if ( class_exists( 'woocommerce' ) ){
  add_action( 'focux_after_header','woocommerce_breadcrumb',10);
}else{
  add_action( 'focux_after_header','focux_breadcrumbs',10);
}

/**
 * Footer
 * @see focux_copyright()	
 */
add_action( 'focux_footer', 'focux_copyright');