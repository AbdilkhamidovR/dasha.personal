=== Focux ===
Contributors: ThemeVan
Requires at least: WordPress 4.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: white, light, two-columns, right-sidebar, responsive-layout, custom-colors, custom-header, custom-menu, featured-images, full-width-template, threaded-comments, accessibility-ready

== Description ==
Focux is a single product eCommerce Theme developed by ThemeVan.

For more information about Focux please go to https://www.themevan.com/item/focux.


== Third-party resources ==

Fontawesome icon font
License: http://fontawesome.io/license/
Source: http://fontawesome.io/