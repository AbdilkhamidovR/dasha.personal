<?php
/**
 * Focux Function File 
 * @package Focux
 */

/** 
 * Load framework init file.
 */
require get_template_directory() . '/inc/init.php';

/**
 * Please Note: Do not add any custom code here. Please use the following methods to customize the theme so that your  customizations aren't lost during updates.
 * 1. Child Theme http://codex.wordpress.org/Child_Themes
 * 2. Install Custom css-js-php plugin  https://wordpress.org/plugins/custom-css-js-php/
 */