<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 * * @package Focux
 */
if(get_theme_mod( 'post_sidebar')=='no' || null == get_theme_mod( 'post_sidebar')){
	get_template_part( 'single', 'no-sidebar' );
	die();
}
get_header(); ?>
	<?php
	  /* Hook: focux_before_content, focux_before_main_content
	   * @Hooked: focux_before_content()
	   * @Hooked: focux_before_main_content();
	   */
	  do_action('focux_before_content');
	  do_action('focux_before_main_content');
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php 
			 get_template_part( 'template-parts/content', 'single' ); 
             echo '<div class="divider"></div>';
			 get_template_part( 'template-parts/single', 'related-posts' ); 
			 get_template_part( 'template-parts/single', 'author' );
			 
			 // If comments are open or we have at least one comment, load up the comment template.
			 if ( comments_open() || get_comments_number() ) :
				comments_template();
			 endif;
			?>
			
			<?php focux_post_navigation(); ?>

		<?php endwhile; // End of the loop. ?>

		<?php
		  /* Hook: focux_after_main_content
		   * @Hooked: focux_after_main_content();
		   */
		  do_action('focux_after_main_content');
		?>
		
		<?php get_sidebar(); ?>
	
	    <?php
	    /* Hook: focux_after_content
	     * @Hooked: focux_after_content();
	     */
		 do_action('focux_after_content');	 
	    ?>
<?php get_footer(); ?>