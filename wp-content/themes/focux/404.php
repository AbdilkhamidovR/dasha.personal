<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 * * @package Focux
 */

get_header(); ?>

	<?php
	  /* Hook: focux_before_content
	   * @Hooked: focux_before_content()
	   */
	  do_action('focux_before_content');
	?>

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'focux' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'focux' ); ?></p>
                    <br />
					<?php get_search_form(); ?>


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		<?php
		  /* Hook: focux_after_content
	       * @Hooked: focux_after_content();
		   */
		  do_action('focux_after_content');
		?>

<?php get_footer(); ?>
