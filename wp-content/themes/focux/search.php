<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 * * @package Focux
 */

get_header(); ?>

	<?php
	  /* Hook: focux_before_content
	   * @Hooked: focux_before_content()
	   */
	  do_action('focux_before_content');
	?>

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h2 class="section-title"><?php printf( esc_html__( 'Search Results for: %s', 'focux' ), '<span>' . get_search_query() . '</span>' ); ?><div class="divider"><span></span></div></h2>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );
				?>

			<?php endwhile; ?>

			<?php echo focux_pagenavi(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>
	
	    <?php
	    /* Hook: focux_after_content
	     * @Hooked: focux_after_content();
	     */
		 do_action('focux_after_content');	 
	    ?>
	
<?php get_footer(); ?>