var scr_path = "domains/dasha.rusadesign.local/wp-content/themes/focux-child/assets/src";
var dist_path = "domains/dasha.rusadesign.local/wp-content/themes/focux-child/assets/dist";

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    // notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    ngmin = require('gulp-ngmin'),
    lr = require('tiny-lr'),
    gulpif = require('gulp-if'),
    connect = require('gulp-connect');
var server = lr();
var sprite = require('css-sprite').stream;

gulp.task('styles', function () {
  return gulp.src(scr_path + '/styles/style.scss')
    .pipe(sass({errLogToConsole: true}))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest(dist_path + '/css'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest(dist_path + '/css'))
    // .pipe(notify({ message: 'Styles task complete' }))
    .pipe(connect.reload());
});

gulp.task('scripts', function () {
  return gulp.src(scr_path + '/scripts/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(ngmin())
    .pipe(gulp.dest(dist_path + '/scripts'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify({mangle: false}))
    .pipe(gulp.dest(dist_path + '/scripts'))
    // .pipe(notify({ message: 'Scripts task complete' }))
    .pipe(connect.reload());
});

gulp.task('images', function () {
  return gulp.src(scr_path + '/images/*.*')
    // .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest(dist_path + '/images'))
    // .pipe(notify({ message: 'Images task complete' }))
    .pipe(connect.reload());
});

gulp.task('clean', function () {
  return gulp.src([dist_path + '/styles', dist_path + '/scripts', dist_path + '/images'], {read: false})
    .pipe(clean());
});

gulp.task('sprites', function () {
  return gulp.src(scr_path + '/images/ico/*.png')
    .pipe(sprite({
      name: 'sprite.png',
      style: '_sprite.scss',
      cssPath: '../images',
      processor: 'scss'
    }))
    .pipe(gulpif('*.png', gulp.dest(dist_path + '/images/'), gulp.dest(scr_path + '/styles/')));
});

gulp.task('html', function () {
  gulp.src(dist_path + '/*.html')
    .pipe(connect.reload());
});

gulp.task('connect', function () {
  connect.server({
    root: 'domains',
    port: 8030,
    livereload: true
  });
});

gulp.task('lr-server', function () {
    server.listen(35729, function (err) {
        if(err) return console.log(err);
    });
});

gulp.task('default', ['connect', 'clean'], function () {
    gulp.start('sprites', 'styles', 'scripts', 'images', 'watch');
});

gulp.task('watch', function () {

  gulp.watch(scr_path + '/styles/**/*.scss', ['styles']);

  gulp.watch(scr_path + '/scripts/**/*.js', ['scripts']);

  gulp.watch(scr_path + '/images/**/*', ['images']);

  gulp.watch(scr_path + '/images/ico/*.png', ['sprites']);

  gulp.watch(dist_path + '/*.html', ['html']);

});