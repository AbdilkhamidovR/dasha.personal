/**
 * theme.js
 * Scripts for the theme.
 * 
 */
jQuery(document).ready(function($){
	
	$('#mobile_menu .menu-item-has-children').addClass('pushy-submenu');
	
	$('.pushy .menu-item-has-children > a').each(function(){
		$(this).after('<a href="javascript:void(0);" class="open-submeu"><i class="fa fa-plus"></i></a>');
	});
	
	$('.menu-toggle').click(function(){
		$('#close-menu').css('display','block');
	});
	
		$('.pushy ul li a.open-submeu').toggle(function(){
			$(this).next().slideDown();
			$(this).children('i').removeClass('fa-plus').addClass('fa-minus');
		},function(){
			$(this).next().slideUp();
			$(this).children('i').removeClass('fa-minus').addClass('fa-plus');
		});
		
	
	// Hide Header on on scroll down
	
	/*Apply the post format to the comment content elements*/
	$('.comment-content').addClass('entry-content');
	
	/*Popup*/
	function fx_popup(button,obj,status){
		$(button).on('click',function(){
			if(status=='open'){
			$('#fx_popup').add('.fx_popup_overlay').add(obj).fadeIn();
			$('body').css('overflow','hidden');
			$('input[type="text"]:first-child').focus();
			}else{
			$('#fx_popup').add('.fx_popup_overlay').add('.popup_content').fadeOut();	
			$('body').css('overflow','auto');
			}
		});
	}
	fx_popup('#product_search','#fx_search','open');
	fx_popup('#product_cart','#fx_cart','open');
	fx_popup('#fx_popup_close','#fx_popup','close');
	
	
	/*Accordion*/
	$('.fx-accordion-section-title').click(function(e) {
		// Grab current anchor value
		var currentAttrValue = $(this).attr('href');
 
		if($(this).is('.active')) {
			$(this).removeClass('active');
			$(this).children('.fx-accordion-section-title span').html('+');
			$(this).next('div').slideDown(300).slideUp(300).removeClass('open');
		}else {
			$(this).addClass('active');
			$(this).next('div').slideDown(300).addClass('open'); 
			$(this).children('.fx-accordion-section-title span').html('&mdash;');
		}
		e.preventDefault();
	});
	
	/*One Page Scroll*/
	$('.bullet_links,.site-header,.anchor_link').localScroll({
		target: 'body', // could be a selector or a jQuery object too.
		queue:true,
		duration:1000,
		hash:true,
		easing:'easeInOutExpo',
		offset: {left: 0, top: -50}
	});	
	
	$(window).scroll(function() {
	
			var currentNode = null;
			$('.vc_row').each(function(){
				if($(this).attr('id')){
				var currentId = $(this).attr('id');	
				if($(window).scrollTop() >= $('#'+currentId).offset().top - 50)
				{
					currentNode = currentId;
				}
				}
			});
			$('.bullet_links').children('a.anchor').removeClass('active');
			$('.bullet_links').find('a[href="#'+currentNode+'"]').addClass('active');
			
		});
	
	/*Masonry*/	
	$('#grid').imagesLoaded(function() {
			$('#grid').masonry({
			itemSelector: '.post',
			gutter: 10
			});
	});


	/*-------------------------
	ligthGallery init	
	--------------------------*/
	$('.wpb_image_grid_ul a').lightGallery({
		selector: 'a',
		selectWithin: '.wpb_image_grid_ul',
		counter: false,
		thumbnail: false,
		share: false,
		download: false,
		hideBarsDelay: 2000,
		appendSubHtmlTo: '.lg-item'
	}); 

});