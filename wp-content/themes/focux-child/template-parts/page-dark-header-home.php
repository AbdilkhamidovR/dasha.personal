<?php
/**
 * Template Name: Dark Header Page (Home Page)
 *
 * This is the template that displays Custom content.
 * @package Focux
 */
get_header(); ?>
	
	<div id="page" class="hfeed site home">
		<div id="content" class="site-content">
			<?php 
				while ( have_posts() ) : the_post(); 
					the_content(); 	
				endwhile; // End of the loop. 

				focux_bullet_links();
			?>
		</div>
	</div>
	
<?php get_footer(); ?>