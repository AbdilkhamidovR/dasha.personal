<?php
/**
 * Template Name: Dark Header Page
 *
 * This is the template that displays Custom content.
 * @package Focux
 */
get_header(); ?>
	
	<div id="page" class="hfeed site portfolio">
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
		<div id="content" class="site-content">
			<?php 
				while ( have_posts() ) : the_post(); 
					the_content(); 	
				endwhile; // End of the loop. 

				focux_bullet_links();
			?>
		</div>
	</div>
	
<?php get_footer(); ?>