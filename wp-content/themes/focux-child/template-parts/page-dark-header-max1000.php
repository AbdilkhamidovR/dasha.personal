<?php
/**
 * Template Name: Dark Header Page (max-width 1000px)
 *
 * This is the template that displays Custom content.
 * @package Focux
 */
get_header(); 

	// do_action('focux_before_content');
?>
	
	<div id="page" class="hfeed site fx-grid-1000">
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
		<div id="content" class="site-content">
			<?php 
				while ( have_posts() ) : the_post(); 
					the_content(); 	
				endwhile; // End of the loop. 

				focux_bullet_links();
			?>
		</div>
	</div>

	<?php //do_action('focux_after_content'); ?>
	
<?php get_footer(); ?>