<?php
/* Functions of Child Theme */

//Define Child Theme URL
define('child_theme_uri', dirname( get_bloginfo('stylesheet_url')) );

add_action( "wp_enqueue_scripts", "theme_enqueue_styles",35);
function theme_enqueue_styles() {
	 wp_enqueue_style( "parent-style", child_theme_uri . "/style.css" );
}

if ( ! function_exists( 'focux_scripts' ) ) :
function focux_scripts() {

	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	$assets_path = child_theme_uri . '/assets';

	wp_enqueue_style( 'focux-style', get_stylesheet_uri(), '', null );
	wp_enqueue_style( 'focux-grid', get_template_directory_uri() .'/inc/css/grid.css', '', null );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() .'/inc/css/font-awesome/css/font-awesome.min.css', '', null );
	wp_enqueue_style( 'swiper', get_template_directory_uri() . '/inc/js/swiper/swiper.min.css', '',null);

	// if ( class_exists( 'woocommerce' ) ){
	// 	wp_enqueue_style( 'focux-woocommerce', child_theme_uri .'/assets/css/woocommerce.css', '', null);
	// }

	// focux-child styles
	// wp_enqueue_style( 'focux-child', get_template_directory_uri() .'/assets/css/main.css', '', '1.0.0' );
	wp_enqueue_style( 'focux-child', child_theme_uri .'/assets/dist/css/style.css', '', '1.0.0' );
	
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/inc/js/modernizr.custom.41385.js', array('jquery'), null, false );
	wp_enqueue_script( 'jquery-ease', get_template_directory_uri() . '/inc/js/jquery.easing.min.js', array('jquery'), null, false );
	wp_enqueue_script( 'focux-pushy', get_template_directory_uri() . '/inc/js/pushy.js', array('jquery'), null, true );
	wp_enqueue_script( 'scrollTo', get_template_directory_uri() . '/inc/js/jquery.scrollTo.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'localScroll', get_template_directory_uri() . '/inc/js/jquery.localScroll.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'jquery-swiper', get_template_directory_uri() . '/inc/js/swiper/swiper.jquery.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'imagesLoaded', get_template_directory_uri() . '/inc/js/jquery.imagesLoaded.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'masonry');


	/**
	 * Pretty Photo Disabling
	 */
	// wp_deregister_script( 'prettyphoto');
	// wp_enqueue_script( 'prettyPhoto-focux-child', $assets_path . '/vendor/prettyphoto/js/jquery.prettyPhoto' . $suffix . '.js', array( 'jquery' ), '3.1.6', true );
	wp_deregister_style( 'prettyphoto' );
	// wp_enqueue_style( 'prettyPhoto-focux-child', $assets_path . '/vendor/prettyphoto/css/prettyphoto.css' );
	wp_deregister_script( 'prettyPhoto' );
	wp_deregister_script( 'prettyphoto' );
	// wp_enqueue_script( 'prettyPhoto-focux-child', $assets_path . '/vendor/prettyphoto/js/jquery.prettyPhoto' . $suffix . '.js', array( 'jquery' ), '3.1.6', true );
	wp_deregister_script( 'prettyPhoto-init' );
	// wp_enqueue_script( 'prettyPhoto-focux-child-init', $assets_path . '/s/jquery.prettyPhoto.init' . $suffix . '.js', array( 'jquery','prettyPhoto-focux-child' ), '3.1.6', true );
	//--
	

	/**
	 * ligthGallery init
	 */
	wp_enqueue_style( 'lightgallery', $assets_path . '/vendor/lightGallery/dist/css/lightgallery.css' );

	wp_enqueue_script( 'lightgallery', $assets_path . '/vendor/lightGallery/dist/js/lightgallery' . $suffix . '.js', array('jquery'), '1.3.7', true );
	wp_enqueue_script( 'lg-fullscreen', $assets_path . '/vendor/lightGallery/demo/js/lg-fullscreen' . $suffix . '.js', array('jquery', 'lightgallery'), '1.0.0', true );
	wp_enqueue_script( 'lg-thumbnail', $assets_path . '/vendor/lightGallery/demo/js/lg-thumbnail' . $suffix . '.js', array('jquery', 'lightgallery'), '1.0.0', true );
	wp_enqueue_script( 'lg-video', $assets_path . '/vendor/lightGallery/demo/js/lg-video' . $suffix . '.js', array('jquery', 'lightgallery'), '1.0.0', true );
	wp_enqueue_script( 'lg-autoplay', $assets_path . '/vendor/lightGallery/demo/js/lg-autoplay' . $suffix . '.js', array('jquery', 'lightgallery'), '1.0.0', true );
	wp_enqueue_script( 'lg-zoom', $assets_path . '/vendor/lightGallery/demo/js/lg-zoom' . $suffix . '.js', array('jquery', 'lightgallery'), '1.0.4', true );
	wp_enqueue_script( 'lg-hash', $assets_path . '/vendor/lightGallery/demo/js/lg-hash' . $suffix . '.js', array('jquery', 'lightgallery'), '1.0.4', true );
	wp_enqueue_script( 'lg-pager', $assets_path . '/vendor/lightGallery/demo/js/lg-pager' . $suffix . '.js', array('jquery', 'lightgallery'), '1.0.4', true );
	wp_enqueue_script( 'mousewheel', $assets_path . '/vendor/lightGallery/lib/jquery.mousewheel.min.js', array('jquery', 'lightgallery'), '3.1.12', true );

	// wp_enqueue_script( 'lightgallery-init', $assets_path . '/vendor/lightGallery/lib/jquery.mousewheel.min.js', array('jquery', 'lightgallery'), '3.1.12', true );
	// -- //

	wp_enqueue_script( 'focux-theme',  child_theme_uri . '/assets/js/theme.js', array('jquery'), null, true );
	// wp_enqueue_script( 'focux-main',  child_theme_uri . '/assets/dist/scripts/main.js', array('jquery'), null, true );
}
endif;

/* focus child after setup */
if ( ! function_exists( 'focux_child_setup' ) ) :
	function focus_child_setup(){

		// setup localisation text domain
		$mo_file_path = child_theme_uri . '/languages/' . get_locale() .'.mo';
		load_textdomain('focux-child', $mo_file_path );
		// ppr($mo_file_path);
		
		// change right top buttons in header
		if( $priority = has_action('focux_header', 'focux_top_buttons') ){
			remove_action( 'focux_header', 'focux_top_buttons', $priority);
		}
		add_action( 'focux_header', 'focux_child_top_buttons', $priority);

		/**
		 * Remove meta
		 */
		add_action('init', 'remove_metas', 100);


		/**
		 * Footer
		 * @see focux_copyright()	
		 */
		remove_action( 'focux_footer', 'focux_copyright'); 
	}
endif;

add_action( 'after_setup_theme', 'focus_child_setup' );



function wp_remove_version() {
	return '';
} 
add_filter('the_generator', 'wp_remove_version');


/**
 * Remove meta
 */
function remove_metas() {
	remove_action('wp_head', array(visual_composer(), 'addMetaData'));
}


/**
 * Top Buttons in Top Navigation
 */
function focux_child_top_buttons(){
	echo'<div id="site-icons" class="fx-grid fx-col3">';
		// echo'<a href="'.esc_url(home_url('/')).'?page_id='.get_option('woocommerce_myaccount_page_id').'"><i class="fa fa-user"></i></a>';
		// echo'<a href="javascript:void(0);" id="product_cart"><i class="fa fa-shopping-cart"></i></a>'; 
		//echo'<a href="javascript:void(0);" id="product_search"><i class="fa fa-search"></i></a>';
		// echo'<div class = "flags_lang_selector">';

		if(function_exists('pll_the_languages')){
			echo '<ul class="lang-selector">';
			pll_the_languages(array('show_names'=>1, 'display_names_as' => slug));
			echo '</ul>';
		}
	echo'</div>';
}


/**
 * Register widget area.
 *
 */
function focux_child_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Bottom Widget 5', 'focux-child' ),
		'id'            => 'bottom-widget-5',
		'description'   => esc_html__('This widget area is placed at the bottom of bottom area.','focux-child'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'focux_child_widgets_init');


add_filter('widget_text', 'do_shortcode');


/**
 * Admin side
 * Filter posts by author
 */
function admin_authors_filter() {
	$parameters = array(
		'name' => 'author', // атрибут name для селекта
		'show_option_all' => 'Все авторы' // лейбл для отображения постов всех авторов
	);
 
	if ( isset($_GET['user']) )
		$parameters['selected'] = $_GET['user']; // выбранный пользователь из списка
 
	wp_dropdown_users( $parameters ); // выводим готовый список
}
 
add_action('restrict_manage_posts', 'admin_authors_filter');


/*
*
my functions
*/
function ppr($_arr, $_ar_name){
	echo "<pre>$_ar_name<br>";
	print_r($_arr);
	echo "</pre>";
}
function fppr($_arr, $_ar_name, $_log_name=false){
	$_log_name = ($_log_name) ? $_log_name : "rlog";
	$str = date("d.m.y H:m:s") . "\r\n" . $_ar_name . "\r\n" . print_r($_arr, true) . "\r\n-------------------\r\n";
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/logs/'.$_log_name.'.log', $str, FILE_APPEND);
}
?>